package pl.szielinski.weight.splash.domain

import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.repository.Repository

class GetIsInitializedUseCase(private val repository: Repository) : DefaultUseCase<Unit, Boolean>() {

  override suspend fun performJob(param: Unit): Boolean {
    return repository.isInitialized()
  }
}
