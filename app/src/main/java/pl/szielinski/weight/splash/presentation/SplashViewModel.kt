package pl.szielinski.weight.splash.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer
import pl.szielinski.weight.splash.view.SplashViewEvent

class SplashViewModel : ViewModel(), ViewEventConsumer<SplashViewEvent> {

    val state = MutableLiveData<SplashModel>()

    private val modelStore = ModelStore(viewModelScope, SplashModel())

    private val eventsChannel = Channel<SplashViewEvent>()

    init {
        modelStore.modelState()
          .onEach {
              state.value = it
          }.launchIn(viewModelScope)

        eventsChannel.consumeAsFlow().onEach {
            lunchAction(it.toAction())
        }.launchIn(viewModelScope)
    }

    override fun onViewEvent(event: SplashViewEvent) {
        eventsChannel.offer(event)
    }

    private fun lunchAction(action: Action<SplashModel>) {
        action.perform().onEach { modelStore.process(it) }.launchIn(viewModelScope)
    }
}
