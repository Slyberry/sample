package pl.szielinski.weight.splash.view

sealed class SplashViewEvent

object NavigatedFromSplashViewEvent : SplashViewEvent()

object NavigatedBackToSplashViewEvent : SplashViewEvent()
