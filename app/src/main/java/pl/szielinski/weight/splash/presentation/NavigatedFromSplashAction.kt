package pl.szielinski.weight.splash.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention

class NavigatedFromSplashAction : Action<SplashModel> {

  override fun perform(): Flow<Intention<SplashModel>> {
    return flow { emit(NavigatedFromSplashIntention()) }
  }
}
