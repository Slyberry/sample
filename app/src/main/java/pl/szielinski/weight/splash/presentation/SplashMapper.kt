package pl.szielinski.weight.splash.presentation

import pl.szielinski.weight.splash.view.NavigatedBackToSplashViewEvent
import pl.szielinski.weight.splash.view.NavigatedFromSplashViewEvent
import pl.szielinski.weight.splash.view.SplashViewEvent

fun SplashViewEvent.toAction() = when(this) {
  NavigatedFromSplashViewEvent -> NavigatedFromSplashAction()
  NavigatedBackToSplashViewEvent -> GetIsInitializedAction()
}
