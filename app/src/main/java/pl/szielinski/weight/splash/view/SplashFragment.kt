package pl.szielinski.weight.splash.view

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.exhaustive
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.szielinski.weight.R
import pl.szielinski.weight.splash.presentation.SplashNavigation
import pl.szielinski.weight.splash.presentation.SplashViewModel

class SplashFragment : ViewEventFragment<SplashViewEvent>(R.layout.splash_fragment) {

    override val viewModel by viewModel<SplashViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.state.observeKt(viewLifecycleOwner) {
            when (it.navigation) {
                SplashNavigation.LOGIN -> {
                    emitToChannel(NavigatedFromSplashViewEvent)
                    findNavController().navigate(SplashFragmentDirections.navigateToLoginFeature())
                }
                SplashNavigation.CURRENT -> {}
                SplashNavigation.MAIN -> findNavController().navigate(SplashFragmentDirections.navigateToMeasureFeature())
            }.exhaustive
        }
    }

    override fun onResume() {
        super.onResume()
        emitToChannel(NavigatedBackToSplashViewEvent)
    }
}
