package pl.szielinski.weight.splash.presentation

data class SplashModel(val navigation: SplashNavigation = SplashNavigation.CURRENT)

enum class SplashNavigation {
  LOGIN, CURRENT, MAIN
}
