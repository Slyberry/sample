package pl.szielinski.weight.splash.presentation

import pl.slyberry.android.common.mvi.Intention

class NavigateToMainSplashIntention : Intention<SplashModel> {

  override fun reduce(oldState: SplashModel): SplashModel {
    return oldState.copy(navigation = SplashNavigation.MAIN)
  }
}

class NavigateToLoginSplashIntention : Intention<SplashModel> {

  override fun reduce(oldState: SplashModel): SplashModel {
    return oldState.copy(navigation = SplashNavigation.LOGIN)
  }
}

class NavigatedFromSplashIntention : Intention<SplashModel> {

  override fun reduce(oldState: SplashModel): SplashModel {
    return oldState.copy(navigation = SplashNavigation.CURRENT)
  }
}
