package pl.szielinski.weight.splash.presentation

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.szielinski.weight.splash.domain.GetIsInitializedUseCase

class GetIsInitializedAction : Action<SplashModel>, KoinComponent {

  private val useCase by inject<GetIsInitializedUseCase>()

  override fun perform(): Flow<Intention<SplashModel>> {
    return useCase.perform(Unit).map {
      when (it) {
        is LoadingResult -> NavigatedFromSplashIntention()
        is SuccessResult -> if (it.data) {
          NavigateToMainSplashIntention()
        } else {
          NavigateToLoginSplashIntention()
        }
        is ErrorResult -> NavigatedFromSplashIntention()
      }
    }
  }
}
