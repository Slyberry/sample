package pl.szielinski.weight

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.szielinski.weight.splash.domain.GetIsInitializedUseCase
import pl.szielinski.weight.splash.presentation.SplashViewModel

val appModule = module {
    viewModel { SplashViewModel() }

    factory { GetIsInitializedUseCase(get()) }
}
