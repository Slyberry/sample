package pl.szielinski.weight

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.slyberry.db.libDbModule
import pl.slyberry.login.featureLoginModule
import pl.slyberry.measure.featureMeasureModule
import pl.slyberry.repository.libRepositoryModule

class WeightApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
        startKoin {
            androidContext(this@WeightApplication)
            modules(
              appModule,
              featureLoginModule,
              featureMeasureModule,
              libRepositoryModule,
              libDbModule
            )
        }
    }
}
