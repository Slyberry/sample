package pl.szielinski.weight

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import pl.szielinski.weight.db.DataManager
import pl.szielinski.weight.db.DataManagerSharedPreferenceHelper

@RunWith(AndroidJUnit4::class)
class DataManagerTest {

    @Test
    @Throws(Exception::class)
    fun testLoadData_endsWithSuccess() {
        val appContext = InstrumentationRegistry.getTargetContext()
        val dataManager = DataManager()

        val dataManagerSharedPreferenceHelper = DataManagerSharedPreferenceHelper(dataManager)
        dataManagerSharedPreferenceHelper.load(appContext)

        checkNotNull( dataManager.workout )
        checkNotNull( dataManager.workout!!.trainings )
        assert( dataManager.workout?.trainings?.size!! > 0 )
    }

    @Test
    @Throws(Exception::class)
    fun testStoreAndLoadData_endsWithSuccess() {
        val appContext = InstrumentationRegistry.getTargetContext()
        val dataManager = DataManager()

        val dataManagerSharedPreferenceHelper = DataManagerSharedPreferenceHelper(dataManager)
        dataManagerSharedPreferenceHelper.load(appContext)
        dataManagerSharedPreferenceHelper.save(appContext)
        dataManagerSharedPreferenceHelper.load(appContext)

        checkNotNull( dataManager.workout )
        checkNotNull( dataManager.workout!!.trainings )
        assert( dataManager.workout?.trainings?.size!! > 0 )
    }
}