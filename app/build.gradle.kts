import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import pl.slyberry.buildsrc.Dependencies
import pl.slyberry.buildsrc.Libraries.implementKoin
import pl.slyberry.buildsrc.Libraries.implementNavigation
import pl.slyberry.buildsrc.Modules

plugins {
    id("com.android.application")
    id("shared-gradle-plugin")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    val properties = gradleLocalProperties(rootDir)
    signingConfigs {
        create("release") {
            storeFile(file("../keystore/weightapp-upload-keystore.jks"))
            storePassword(properties.getProperty("keystore_key"))
            keyAlias("upload")
            keyPassword(properties.getProperty("password_key"))
        }
    }
    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(project(Modules.LIB_UI))
    implementation(project(Modules.LIB_COMMON))
    implementation(project(Modules.FEATURE_LOGIN))
    implementation(project(Modules.LIB_REPOSITORY))
    implementation(project(Modules.LIB_DB))
    implementation(project(Modules.FEATURE_MEASURE))

    implementKoin()
    implementNavigation()

    implementation(Dependencies.DATE)
    implementation(Dependencies.LIVE_DATA)
    implementation(Dependencies.COORDINATOR)
    implementation(Dependencies.LIVE_EVENT)
}
