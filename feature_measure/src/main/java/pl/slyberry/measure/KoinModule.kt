package pl.slyberry.measure

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.slyberry.measure.add.domain.AddWeightUseCase
import pl.slyberry.measure.add.domain.GetWeightParamsUseCase
import pl.slyberry.measure.add.presentation.AddMeasureViewModel
import pl.slyberry.measure.goal.domain.SetGoalUseCase
import pl.slyberry.measure.goal.presentation.GoalsViewModel
import pl.slyberry.measure.list.domain.DeleteWeightUseCase
import pl.slyberry.measure.list.domain.GetWeightHistoryUseCase
import pl.slyberry.measure.list.presentation.MeasuresListViewModel
import pl.slyberry.measure.main.domain.GetBmiAndWeightUseCase
import pl.slyberry.measure.main.domain.GetNameUseCase
import pl.slyberry.measure.main.presentation.MenuViewModel

val featureMeasureModule = module {
  viewModel { MeasuresListViewModel() }
  viewModel { AddMeasureViewModel() }
  viewModel { GoalsViewModel() }
  viewModel { MenuViewModel() }

  factory { DeleteWeightUseCase(get()) }
  factory { GetWeightHistoryUseCase(get()) }
  factory { GetWeightParamsUseCase(get()) }
  factory { AddWeightUseCase(get()) }
  factory { GetBmiAndWeightUseCase(get()) }
  factory { SetGoalUseCase(get()) }
  factory { GetNameUseCase(get()) }
}
