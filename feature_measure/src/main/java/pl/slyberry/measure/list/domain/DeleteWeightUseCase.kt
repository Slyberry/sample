package pl.slyberry.measure.list.domain

import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.repository.Repository

class DeleteWeightUseCase(private val repository: Repository) : DefaultUseCase<Long, Unit>() {

  override suspend fun performJob(param: Long) {
    if (repository.getWeightItemCount() <= 1) {
      throw Exception() //todo some name
    }
    repository.deleteWeightItem(param)
  }
}
