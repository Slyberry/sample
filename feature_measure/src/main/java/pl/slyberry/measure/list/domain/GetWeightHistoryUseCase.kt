package pl.slyberry.measure.list.domain

import kotlinx.coroutines.flow.Flow
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.WeightHistoryItem
import pl.slyberry.repository.Repository

class GetWeightHistoryUseCase(private val repository: Repository) : DefaultUseCase<Unit, Flow<List<WeightHistoryItem>>>() {

  override suspend fun performJob(param: Unit): Flow<List<WeightHistoryItem>> {
    return repository.getWeightHistory()
  }
}
