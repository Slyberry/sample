package pl.slyberry.measure.list.presentation

import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.WeightHistoryItem
import pl.slyberry.domain.mvi.Status
import pl.slyberry.measure.list.presentation.MeasuresListModel

class GetHistoryLoadingIntention : Intention<MeasuresListModel> {

  override fun reduce(oldState: MeasuresListModel): MeasuresListModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class GetHistorySuccessIntention(private val items: List<WeightHistoryItem>) : Intention<MeasuresListModel> {

  override fun reduce(oldState: MeasuresListModel): MeasuresListModel {
    return oldState.copy(items = items, status = Status.IDLE)
  }
}

class GetHistoryErrorIntention : Intention<MeasuresListModel> {

  override fun reduce(oldState: MeasuresListModel): MeasuresListModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class DeleteHistoryItemLoadingIntention : Intention<MeasuresListModel> {

  override fun reduce(oldState: MeasuresListModel): MeasuresListModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class DeleteHistoryItemSuccessIntention : Intention<MeasuresListModel> {

  override fun reduce(oldState: MeasuresListModel): MeasuresListModel {
    return oldState.copy(status = Status.IDLE)
  }
}

class DeleteHistoryItemErrorIntention : Intention<MeasuresListModel> {

  override fun reduce(oldState: MeasuresListModel): MeasuresListModel {
    return oldState.copy(status = Status.ERROR)
  }
}
