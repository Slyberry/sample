package pl.slyberry.measure.list.presentation

import pl.slyberry.measure.list.view.DeleteWeightViewEvent

fun DeleteWeightViewEvent.toAction() = DeleteWeightAction(id)
