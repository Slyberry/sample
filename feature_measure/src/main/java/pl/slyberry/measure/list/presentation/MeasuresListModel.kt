package pl.slyberry.measure.list.presentation

import pl.slyberry.domain.WeightHistoryItem
import pl.slyberry.domain.mvi.Status

data class MeasuresListModel(val items: List<WeightHistoryItem>, val status: Status) {

  companion object {
    fun default() = MeasuresListModel(emptyList(), Status.IDLE)
  }
}
