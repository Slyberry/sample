package pl.slyberry.measure.list.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer
import pl.slyberry.measure.list.view.DeleteWeightViewEvent

class MeasuresListViewModel : ViewModel(), ViewEventConsumer<DeleteWeightViewEvent> {

  val state = MutableLiveData<MeasuresListModel>()

  private val modelStore = ModelStore(viewModelScope, MeasuresListModel.default())

  private val eventsChannel = Channel<DeleteWeightViewEvent>()

  init {
    modelStore.modelState()
      .onEach {
        state.value = it
      }.launchIn(viewModelScope)

    eventsChannel.consumeAsFlow().onEach {
      lunchAction(it.toAction())
    }.launchIn(viewModelScope)

    lunchAction(GetWeightHistoryAction())
  }

  override fun onViewEvent(event: DeleteWeightViewEvent) {
    eventsChannel.offer(event)
  }

  private fun lunchAction(action: Action<MeasuresListModel>) {
    action.perform().onEach { modelStore.process(it) }.launchIn(viewModelScope)
  }
}
