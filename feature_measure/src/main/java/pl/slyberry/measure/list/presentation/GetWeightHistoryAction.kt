package pl.slyberry.measure.list.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.measure.list.domain.GetWeightHistoryUseCase

class GetWeightHistoryAction : Action<MeasuresListModel>, KoinComponent {

  private val useCase by inject<GetWeightHistoryUseCase>()

  override fun perform(): Flow<Intention<MeasuresListModel>> {
    return useCase.perform(Unit).flatMapLatest {
      when (it){
        is LoadingResult -> flow { GetHistoryLoadingIntention() }
        is SuccessResult -> it.data.map { GetHistorySuccessIntention(it) }
        is ErrorResult -> flow { GetHistoryErrorIntention() }
      }
    }
  }
}
