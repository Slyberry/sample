package pl.slyberry.measure.list.presentation

import pl.slyberry.domain.WeightHistoryItem
import pl.slyberry.ui.android.Label
import pl.slyberry.ui.view.measure.history.WeightMeasureItemChroma
import pl.slyberry.ui.view.measure.history.WeightMeasureItemViewEntity

fun createViewEntity(items: List<WeightHistoryItem>): List<WeightMeasureItemViewEntity> {
  return items.mapIndexed { i, it ->
    WeightMeasureItemViewEntity(
      it.id,
      Label.create(it.date.toDisplayString()),
      Label.create(it.weight.value.toString()),
      Label.create("${it.change.value} kg"),
      createBackgroundChroma(i)
    )
  }
}

private fun createBackgroundChroma(index: Int): WeightMeasureItemChroma {
  return when {
    index%2 == 0 -> WeightMeasureItemChroma.EVEN
    else -> WeightMeasureItemChroma.ODD
  }
}
