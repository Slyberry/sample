package pl.slyberry.measure.list.view

import android.os.Bundle
import android.view.View
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.domain.mvi.Status
import pl.slyberry.measure.R
import pl.slyberry.measure.databinding.MeasuresListFragmentBinding
import pl.slyberry.measure.list.presentation.MeasuresListViewModel
import pl.slyberry.measure.list.presentation.createViewEntity

class MeasuresListFragment : ViewEventFragment<DeleteWeightViewEvent>(R.layout.measures_list_fragment) {

  override val viewModel by viewModel<MeasuresListViewModel>()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with(MeasuresListFragmentBinding.bind(view)) {
      toolbar.buttonBack.setOnClickListener { navigateUp() }
      toolbar.toolbarTitle.setText(R.string.measures_list_s_history)

      val adapter = MeasuresListAdapter()
      adapter.onItemClickListener = {
        emitToChannel(DeleteWeightViewEvent(it.id))
      }
      measuresList.adapter = adapter

      viewModel.state.observeKt(viewLifecycleOwner) {
        adapter.items = createViewEntity(it.items)
        if (it.status == Status.ERROR) {
          Snackbar.make(view, "You cannot delete the last item.", Snackbar.LENGTH_SHORT).show()
        }
      }
    }
  }
}
