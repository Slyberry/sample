package pl.slyberry.measure.list.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.measure.list.domain.DeleteWeightUseCase

class DeleteWeightAction(private val id: Long) : Action<MeasuresListModel>, KoinComponent {

  private val useCase by inject<DeleteWeightUseCase>()

  override fun perform(): Flow<Intention<MeasuresListModel>> {
    return useCase.perform(id).map {
      when (it){
        is LoadingResult -> DeleteHistoryItemLoadingIntention()
        is SuccessResult -> DeleteHistoryItemSuccessIntention()
        is ErrorResult -> DeleteHistoryItemErrorIntention()
      }
    }
  }
}
