package pl.slyberry.measure.list.view

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pl.slyberry.ui.recyclerview.SimpleRecyclerViewHolder
import pl.slyberry.ui.view.measure.history.WeightMeasureItemViewEntity
import pl.slyberry.ui.view.measure.history.WeightMeasureItemViewHolder

class MeasuresListAdapter : RecyclerView.Adapter<SimpleRecyclerViewHolder>() {

  var onItemClickListener: ((DeleteWeightEvent) -> Unit)? = null

  var items = emptyList<WeightMeasureItemViewEntity>()
    set(value) {
      field = value
      notifyDataSetChanged()
    }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleRecyclerViewHolder {
    return WeightMeasureItemViewHolder(parent)
  }

  override fun getItemCount() = items.size

  override fun onBindViewHolder(holder: SimpleRecyclerViewHolder, position: Int) {
    with ((holder as WeightMeasureItemViewHolder)) {
      applyViewEntity(items[position]) {
        onItemClickListener?.invoke(DeleteWeightEvent(it))
      }
    }
  }
}
