package pl.slyberry.measure.add.presentation

import pl.slyberry.domain.Weight

fun AddMeasureViewEvent.toAction() = AddWeightAction(Weight(value))
