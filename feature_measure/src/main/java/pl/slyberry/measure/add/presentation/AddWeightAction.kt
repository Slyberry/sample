package pl.slyberry.measure.add.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.domain.Weight
import pl.slyberry.measure.add.domain.AddWeightUseCase

class AddWeightAction(private val weight: Weight) : Action<AddMeasureModel>, KoinComponent {

  private val useCase by inject<AddWeightUseCase>()

  override fun perform(): Flow<Intention<AddMeasureModel>> {
    return useCase.perform(weight).map {
      when(it) {
        is LoadingResult -> AddWeightLoadingIntention()
        is SuccessResult -> AddWeightSuccessIntention()
        is ErrorResult -> AddWeightErrorIntention()
      }
    }
  }
}
