package pl.slyberry.measure.add.presentation

import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.WeightParams
import pl.slyberry.domain.mvi.BackNavigation
import pl.slyberry.domain.mvi.Status

class AddWeightLoadingIntention : Intention<AddMeasureModel> {

  override fun reduce(oldState: AddMeasureModel): AddMeasureModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class AddWeightSuccessIntention : Intention<AddMeasureModel> {

  override fun reduce(oldState: AddMeasureModel): AddMeasureModel {
    return oldState.copy(status = Status.IDLE, navigation = BackNavigation.BACK)
  }
}

class AddWeightErrorIntention : Intention<AddMeasureModel> {

  override fun reduce(oldState: AddMeasureModel): AddMeasureModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class GetWeightAndHeightLoadingIntention : Intention<AddMeasureModel> {

  override fun reduce(oldState: AddMeasureModel): AddMeasureModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class GetWeightAndHeightSuccessIntention(private val weightParams: WeightParams) : Intention<AddMeasureModel> {

  override fun reduce(oldState: AddMeasureModel): AddMeasureModel {
    return oldState.copy(weightParams = weightParams, status = Status.IDLE)
  }
}

class GetWeightAndHeightErrorIntention : Intention<AddMeasureModel> {

  override fun reduce(oldState: AddMeasureModel): AddMeasureModel {
    return oldState.copy(status = Status.ERROR)
  }
}
