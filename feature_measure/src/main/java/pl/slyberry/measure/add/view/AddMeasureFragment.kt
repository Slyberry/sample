package pl.slyberry.measure.add.view

import android.os.Bundle
import android.view.View
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.exhaustive
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.domain.mvi.BackNavigation
import pl.slyberry.measure.R
import pl.slyberry.measure.add.presentation.AddMeasureViewEvent
import pl.slyberry.measure.add.presentation.AddMeasureViewModel
import pl.slyberry.measure.add.presentation.toViewEntity
import pl.slyberry.measure.databinding.AddMeasureFragmentBinding

class AddMeasureFragment : ViewEventFragment<AddMeasureViewEvent>(R.layout.add_measure_fragment) {

  override val viewModel by viewModel<AddMeasureViewModel>()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with (AddMeasureFragmentBinding.bind(view)) {
      toolbar.buttonBack.setOnClickListener { navigateUp() }
      toolbar.toolbarTitle.text = getString(R.string.add_measure_s_title)

      viewModel.state.observeKt(viewLifecycleOwner) {
        handleNavigation(it.navigation)
        it.toViewEntity()?.let { weightRuler.applyViewEntity(it) }
      }

      buttonAdd.setOnClickListener { emitToChannel(AddMeasureViewEvent(weightRuler.selectedValue)) }
    }
  }

  private fun handleNavigation(navigation: BackNavigation) {
    when (navigation) {
      BackNavigation.CURRENT -> Unit
      BackNavigation.BACK -> navigateUp()
    }.exhaustive
  }
}
