package pl.slyberry.measure.add.domain

import pl.slyberry.android.common.usecase.Completed
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.Weight
import pl.slyberry.repository.Repository

class AddWeightUseCase(private val repository: Repository) : DefaultUseCase<Weight, Completed>() {

  override suspend fun performJob(param: Weight): Completed {
    repository.addWeight(param)
    return Completed
  }
}
