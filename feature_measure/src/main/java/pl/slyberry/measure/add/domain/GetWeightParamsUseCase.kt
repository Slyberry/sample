package pl.slyberry.measure.add.domain

import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.WeightParams
import pl.slyberry.repository.Repository

class GetWeightParamsUseCase(private val repository: Repository) : DefaultUseCase<Unit, WeightParams>() {

  override suspend fun performJob(param: Unit): WeightParams {
    return repository.getRecentWeightParams()
  }
}
