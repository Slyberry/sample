package pl.slyberry.measure.add.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.measure.add.domain.GetWeightParamsUseCase

class GetWeightParamsAction : Action<AddMeasureModel>, KoinComponent {

  private val useCase by inject<GetWeightParamsUseCase>()

  override fun perform(): Flow<Intention<AddMeasureModel>> {
    return useCase.perform(Unit).map {
      when(it) {
        is LoadingResult -> GetWeightAndHeightLoadingIntention()
        is SuccessResult -> GetWeightAndHeightSuccessIntention(it.data)
        is ErrorResult -> GetWeightAndHeightErrorIntention()
      }
    }
  }
}
