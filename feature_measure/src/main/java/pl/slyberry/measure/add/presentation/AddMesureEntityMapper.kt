package pl.slyberry.measure.add.presentation

import pl.slyberry.ui.view.ruler.weight.WeightViewEntity

fun AddMeasureModel.toViewEntity(): WeightViewEntity? {
  return weightParams?.let {
    WeightViewEntity(it.current.value.toInt(), it.max.value.toInt())
  }
}
