package pl.slyberry.measure.add.presentation

import pl.slyberry.domain.WeightParams
import pl.slyberry.domain.mvi.BackNavigation
import pl.slyberry.domain.mvi.Status

data class AddMeasureModel(val weightParams: WeightParams?, val status: Status, val navigation: BackNavigation) {

  companion object {
    fun default() = AddMeasureModel(null, Status.IDLE, BackNavigation.CURRENT)
  }
}
