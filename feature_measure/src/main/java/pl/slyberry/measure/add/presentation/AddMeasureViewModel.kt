package pl.slyberry.measure.add.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer

class AddMeasureViewModel : ViewModel(), ViewEventConsumer<AddMeasureViewEvent> {

  val state = MutableLiveData<AddMeasureModel>()

  private val modelStore = ModelStore(viewModelScope, AddMeasureModel.default())

  private val eventsChannel = Channel<AddMeasureViewEvent>()

  init {
    modelStore.modelState()
      .onEach {
        state.value = it
      }.launchIn(viewModelScope)

    eventsChannel.consumeAsFlow().onEach {
      lunchAction(it.toAction())
    }.launchIn(viewModelScope)

    lunchAction(GetWeightParamsAction())
  }

  override fun onViewEvent(event: AddMeasureViewEvent) {
    eventsChannel.offer(event)
  }

  private fun lunchAction(action: Action<AddMeasureModel>) {
    action.perform().onEach { modelStore.process(it) }.launchIn(viewModelScope)
  }
}
