package pl.slyberry.measure.main.domain

import pl.slyberry.domain.Height
import pl.slyberry.domain.Weight
import kotlin.math.pow

data class BmiCalculator(val weight: Weight, val height: Height) {

  fun calculate(): Bmi = Bmi(weight.value.div(height.value.div(100).pow(2)))
}
