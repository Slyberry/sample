package pl.slyberry.measure.main.presentation

import pl.slyberry.domain.Name
import pl.slyberry.domain.WeightProgress
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status
import pl.slyberry.measure.main.domain.WeightAndBmi

data class MenuModel(
  val name: Name?,
  val weightAndBmi: WeightAndBmi?,
  val progress: WeightProgress?,
  val status: Status,
  val navigation: Navigation
) {

  companion object {
    fun default() = MenuModel(null, null, null, Status.IDLE, Navigation.CURRENT)
  }
}
