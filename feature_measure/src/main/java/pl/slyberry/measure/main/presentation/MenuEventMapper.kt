package pl.slyberry.measure.main.presentation

import pl.slyberry.measure.main.view.MenuViewEvent

fun MenuViewEvent.toAction() = GetBmiAndWeightAction()
