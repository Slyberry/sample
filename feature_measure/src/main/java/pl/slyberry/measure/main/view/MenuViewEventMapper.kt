package pl.slyberry.measure.main.view

import pl.slyberry.domain.WeightProgress
import pl.slyberry.measure.main.domain.BmiDate
import pl.slyberry.measure.main.domain.WeightDate
import pl.slyberry.ui.android.Label
import pl.slyberry.ui.view.measure.param.MeasuredParamViewEntity
import pl.slyberry.ui.view.measure.progress.GoalProgressViewEntity

fun BmiDate.toViewEntity() = MeasuredParamViewEntity(
  Label.create("BMI"),
  Label.create(String.format("%.1f", bmi.value)),
  Label.create("kg/m2"),
  Label.create(date.toDisplayString())
)

fun WeightDate.toViewEntity() = MeasuredParamViewEntity(
  Label.create("Last Weight"),
  Label.create(weight.value.toString()),
  Label.create("kg"),
  Label.create(date.toDisplayString())
)

fun WeightProgress.toViewEntity(): GoalProgressViewEntity? {
    val progressViewEntity = GoalProgressViewEntity(
            start.value,
            current.value,
            target.value
    )
    return if (progressViewEntity.progress == 100) {
        null
    } else {
        progressViewEntity
    }
}
