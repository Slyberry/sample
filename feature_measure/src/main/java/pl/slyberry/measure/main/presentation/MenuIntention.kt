package pl.slyberry.measure.main.presentation

import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.Name
import pl.slyberry.domain.WeightProgress
import pl.slyberry.domain.mvi.Status
import pl.slyberry.measure.main.domain.WeightAndBmi

class GetMenuDataLoadingIntention : Intention<MenuModel> {

  override fun reduce(oldState: MenuModel): MenuModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class GetMenuDataSuccessIntention(private val weightAndBmi: WeightAndBmi) : Intention<MenuModel> {

  override fun reduce(oldState: MenuModel): MenuModel {
    return oldState.copy(weightAndBmi = weightAndBmi, status = Status.IDLE)
  }
}

class GetMenuDataErrorIntention : Intention<MenuModel> {

  override fun reduce(oldState: MenuModel): MenuModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class GetCurrentProgressSuccessIntention(private val progress: WeightProgress) : Intention<MenuModel> {

  override fun reduce(oldState: MenuModel): MenuModel {
    return oldState.copy(progress = progress)
  }
}

class GetNameSuccessIntention(private val name: Name) : Intention<MenuModel> {

  override fun reduce(oldState: MenuModel): MenuModel {
    return oldState.copy(name = name)
  }
}
