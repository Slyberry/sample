package pl.slyberry.measure.main.view

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.makeGone
import pl.slyberry.android.common.ext.makeVisible
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.android.common.viewbinding.viewBinding
import pl.slyberry.measure.R
import pl.slyberry.measure.databinding.MeasureMenuFragmentBinding
import pl.slyberry.measure.main.presentation.MenuModel
import pl.slyberry.measure.main.presentation.MenuViewModel
import pl.slyberry.ui.android.Label
import pl.slyberry.ui.android.setLabel
import pl.slyberry.ui.font.FontSpan
import pl.slyberry.ui.view.measure.progress.GoalProgressViewEntity

class MenuFragment : ViewEventFragment<MenuViewEvent>(R.layout.measure_menu_fragment) {

  override val viewModel by viewModel<MenuViewModel>()

  private val binding by viewBinding(MeasureMenuFragmentBinding::bind)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with (binding) {
      toolbar.buttonBack.makeGone()
      toolbar.toolbarTitle.setText(R.string.menu_s_title)
      weightParam.setOnClickListener { findNavController().navigate(MenuFragmentDirections.navigateToMeasureListFragment()) }
      buttonAdd.setOnClickListener { findNavController().navigate(MenuFragmentDirections.navigateToAddMeasureDialog()) }
      requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
          requireActivity().finish()
        }
      })

      viewModel.model.observeKt(viewLifecycleOwner) {
        applyModel(it)
      }
    }
  }

  private fun applyModel(model: MenuModel) {
    with (binding) {
      model.weightAndBmi?.let {
        weightParam.applyViewEntity(it.weight.toViewEntity())
        bmiParam.applyViewEntity(it.bmi.toViewEntity())
      }
      model.progress?.toViewEntity()?.let {
        applyProgressViewEntity(it, model.name?.value)
      } ?: hideProgressViewEntity()
    }
  }

  private fun getNameText(name: String): SpannableString {
    val text = "Hi, $name! You are doing great!"
    return SpannableString(text).apply {
      setSpan(
        FontSpan(ResourcesCompat.getFont(requireContext(), R.font.montserrat_bold)),
        text.length - 6,
        text.length,
        Spannable.SPAN_INCLUSIVE_INCLUSIVE
      )
    }
  }

  private fun applyProgressViewEntity(viewEntity: GoalProgressViewEntity, name: String?) {
    with (binding) {
      progressView.applyViewEntity(viewEntity)
      name
        ?.let { dailyInfo.text = getNameText(it) }
        ?: dailyInfo.setLabel(Label.createHtml(R.string.menu_s_welcome_goal_set))
      progressView.makeVisible()

      icon.setOnClickListener { }
      dailyInfo.setOnClickListener { }
    }
  }

  private fun hideProgressViewEntity() {
    with (binding) {
      dailyInfo.setText(R.string.menu_s_welcome_goal_not_set)
      progressView.makeGone()

      icon.setOnClickListener { findNavController().navigate(MenuFragmentDirections.navigateToChangeGoal()) }
      dailyInfo.setOnClickListener { findNavController().navigate(MenuFragmentDirections.navigateToChangeGoal()) }
    }
  }
}
