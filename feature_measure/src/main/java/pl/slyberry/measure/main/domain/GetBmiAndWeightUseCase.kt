package pl.slyberry.measure.main.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.zip
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.repository.Repository

class GetBmiAndWeightUseCase(private val repository: Repository) : DefaultUseCase<Unit, Flow<WeightAndBmi>>() {

  override suspend fun performJob(param: Unit): Flow<WeightAndBmi> {
    return repository.getRecentHeightHistoryItem().combine(repository.getRecentWeightHistoryItem()) { height, weight ->
      val bmiCalculator = BmiCalculator(weight.weight, height.height)
      WeightAndBmi(
              BmiDate(bmiCalculator.calculate(), maxOf(weight.date, height.date)),
              WeightDate(weight.weight, weight.date)
      )
    }
  }
}
