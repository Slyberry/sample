package pl.slyberry.measure.main.domain

import pl.slyberry.domain.DateWrapper

data class Bmi(val value: Float)

data class BmiDate(val bmi: Bmi, val date: DateWrapper)
