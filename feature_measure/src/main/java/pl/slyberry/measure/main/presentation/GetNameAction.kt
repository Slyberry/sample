package pl.slyberry.measure.main.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.measure.main.domain.GetNameUseCase

class GetNameAction : Action<MenuModel>, KoinComponent {

  private val useCase by inject<GetNameUseCase>()

  override fun perform(): Flow<Intention<MenuModel>> {
    return useCase.perform(Unit).map {
      when (it) {
        is LoadingResult -> GetMenuDataLoadingIntention()
        is SuccessResult -> GetNameSuccessIntention(it.data)
        is ErrorResult -> GetMenuDataErrorIntention()
      }
    }
  }
}