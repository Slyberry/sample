package pl.slyberry.measure.main.presentation

import kotlinx.coroutines.flow.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.measure.main.domain.GetBmiAndWeightUseCase

class GetBmiAndWeightAction : Action<MenuModel>, KoinComponent {

  private val useCase by inject<GetBmiAndWeightUseCase>()

  override fun perform(): Flow<Intention<MenuModel>> {
    return useCase.perform(Unit).flatMapLatest {
      when (it) {
        is LoadingResult -> flow { GetMenuDataLoadingIntention() }
        is SuccessResult -> it.data.map { GetMenuDataSuccessIntention(it) }
        is ErrorResult -> flow { GetMenuDataErrorIntention() }
      }
    }
  }
}
