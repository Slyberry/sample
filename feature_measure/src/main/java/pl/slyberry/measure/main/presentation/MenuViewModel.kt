package pl.slyberry.measure.main.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer
import pl.slyberry.measure.main.view.MenuViewEvent

class MenuViewModel : ViewModel(), ViewEventConsumer<MenuViewEvent> {

  val model = MutableLiveData<MenuModel>()

  private val modelStore = ModelStore(viewModelScope, MenuModel.default())

  private val eventsChannel = Channel<MenuViewEvent>()

  init {
    modelStore.modelState()
      .onEach {
          model.value = it
      }.launchIn(viewModelScope)

    eventsChannel.consumeAsFlow().onEach {
      lunchAction(it.toAction())
    }.launchIn(viewModelScope)

    lunchAction(GetNameAction())
    lunchAction(GetBmiAndWeightAction())
    lunchAction(GetCurrentGoalAction())
  }

  override fun onViewEvent(event: MenuViewEvent) {
    eventsChannel.offer(event)
  }

  private fun lunchAction(action: Action<MenuModel>) {
    action.perform().onEach {
      modelStore.process(it)
    }.launchIn(viewModelScope)
  }
}
