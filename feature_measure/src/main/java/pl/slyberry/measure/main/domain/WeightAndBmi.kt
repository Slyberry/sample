package pl.slyberry.measure.main.domain

data class WeightAndBmi(val bmi: BmiDate, val weight: WeightDate)
