package pl.slyberry.measure.main.domain

import pl.slyberry.domain.DateWrapper
import pl.slyberry.domain.Weight

data class WeightDate(val weight: Weight, val date: DateWrapper)
