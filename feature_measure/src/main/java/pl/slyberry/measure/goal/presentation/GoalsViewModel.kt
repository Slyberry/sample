package pl.slyberry.measure.goal.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer
import pl.slyberry.measure.goal.view.SetGoalViewEvent

class GoalsViewModel : ViewModel(), ViewEventConsumer<SetGoalViewEvent> {

  val state = MutableLiveData<GoalsModel>()

  private val modelStore = ModelStore(viewModelScope, GoalsModel.default())

  private val eventsChannel = Channel<SetGoalViewEvent>()

  init {
    modelStore.modelState()
      .onEach {
        state.value = it
      }.launchIn(viewModelScope)

    eventsChannel.consumeAsFlow().onEach {
      lunchAction(it.toAction())
    }.launchIn(viewModelScope)

    lunchAction(GetAverageBodyParamsAction())
  }

  override fun onViewEvent(event: SetGoalViewEvent) {
    eventsChannel.offer(event)
  }

  private fun lunchAction(action: Action<GoalsModel>) {
    action.perform().onEach { modelStore.process(it) }.launchIn(viewModelScope)
  }
}
