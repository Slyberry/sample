package pl.slyberry.measure.goal.view

data class SetGoalViewEvent(val goal: SelectedGoal, val value: Int)

enum class SelectedGoal {
  LOSS, KEEP, GAIN
}
