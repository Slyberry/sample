package pl.slyberry.measure.goal.presentation

import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.domain.mvi.BackNavigation
import pl.slyberry.domain.mvi.Status

data class GoalsModel(val weightAndHeight: WeightAndHeightStatistics?, val status: Status, val navigation: BackNavigation) {

  companion object {
    fun default() = GoalsModel(null, Status.IDLE, BackNavigation.CURRENT)
  }
}
