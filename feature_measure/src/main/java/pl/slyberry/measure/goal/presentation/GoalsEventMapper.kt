package pl.slyberry.measure.goal.presentation

import pl.slyberry.android.common.mvi.Action
import pl.slyberry.domain.GoalCreationData
import pl.slyberry.domain.Weight
import pl.slyberry.domain.WeightGoalType
import pl.slyberry.measure.goal.view.SelectedGoal
import pl.slyberry.measure.goal.view.SetGoalViewEvent

fun SetGoalViewEvent.toAction(): Action<GoalsModel> {
  val type = when (goal) {
    SelectedGoal.LOSS -> WeightGoalType.LOSS
    SelectedGoal.KEEP -> WeightGoalType.KEEP
    SelectedGoal.GAIN -> WeightGoalType.GAIN
  }
  return SetGoalAction(GoalCreationData(Weight(value), type))
}
