package pl.slyberry.measure.goal.view

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.*
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.domain.mvi.BackNavigation
import pl.slyberry.measure.R
import pl.slyberry.measure.databinding.MeasureGoalsFragmentBinding
import pl.slyberry.measure.goal.presentation.GoalsModel
import pl.slyberry.measure.goal.presentation.GoalsViewModel
import pl.slyberry.ui.view.ruler.weight.WeightViewEntity

internal class GoalsFragment : ViewEventFragment<SetGoalViewEvent>(R.layout.measure_goals_fragment) {

  override val viewModel by viewModel<GoalsViewModel>()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with(MeasureGoalsFragmentBinding.bind(view)) {
      toolbar.buttonBack.setOnClickListener { findNavController().navigateUp() }
      viewModel.state.observeKt(viewLifecycleOwner) {
        handleNavigation(it)

        (it.weightAndHeight)?.let {
          weightRuler.applyViewEntity(WeightViewEntity(it.averageWeight.value.toInt(), it.maxWeight.value.toInt()))
        }
      }

      buttonNext.setOnClickListener {
        val goal = when (goalsRadioGroup.checkedRadioButtonId) {
          R.id.goal_loss_button -> SelectedGoal.LOSS
          R.id.goal_keep_button -> SelectedGoal.KEEP
          R.id.goal_gain_button -> SelectedGoal.GAIN
          else -> throw IllegalArgumentException("Not implemented.")
        }
        emitToChannel(SetGoalViewEvent(goal, weightRuler.selectedValue))
      }

      goalsRadioGroup.setOnCheckedChangeListener { _, checkedId ->
        when (checkedId) {
          R.id.goal_loss_button -> weightRuler.makeVisible()
          R.id.goal_keep_button -> weightRuler.makeInvisible()
          R.id.goal_gain_button -> weightRuler.makeVisible()
        }
      }
    }
  }

  private fun handleNavigation(model: GoalsModel) {
    when (model.navigation) {
      BackNavigation.BACK -> navigateUp()
      BackNavigation.CURRENT -> Unit
    }.exhaustive
  }
}
