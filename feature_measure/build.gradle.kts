import pl.slyberry.buildsrc.Dependencies
import pl.slyberry.buildsrc.Libraries.implementKoin
import pl.slyberry.buildsrc.Libraries.implementNavigation
import pl.slyberry.buildsrc.Modules

plugins {
    id("com.android.library")
    id("shared-gradle-plugin")
    id("androidx.navigation.safeargs.kotlin")
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(project(Modules.LIB_UI))
    implementation(project(Modules.LIB_COMMON))
    implementation(project(Modules.LIB_DOMAIN))
    implementation(project(Modules.LIB_REPOSITORY))

    implementKoin()
    implementNavigation()

    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.COORDINATOR)
    implementation(Dependencies.LIVE_EVENT)
}
