package pl.slyberry.login.gender.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer
import pl.slyberry.login.gender.view.GenderViewEvent

internal class GenderViewModel : ViewModel(), ViewEventConsumer<GenderViewEvent> {

  val model = MutableLiveData<GenderModel>()

  private val modelStore = ModelStore(viewModelScope, GenderModel.default())

  private val eventsChannel = Channel<GenderViewEvent>()

  init {
    modelStore.modelState()
      .onEach {
        model.value = it
      }.launchIn(viewModelScope)

    eventsChannel.consumeAsFlow().onEach {
      lunchAction(it.toAction())
    }.launchIn(viewModelScope)

    lunchAction(GetNameAction())
  }

  override fun onViewEvent(event: GenderViewEvent) {
    eventsChannel.offer(event)
  }

  private fun lunchAction(action: Action<GenderModel>) {
    action.perform().onEach { modelStore.process(it) }.launchIn(viewModelScope)
  }
}
