package pl.slyberry.login.gender.presentation

import pl.slyberry.android.common.mvi.Action
import pl.slyberry.domain.BirthDate
import pl.slyberry.domain.Gender
import pl.slyberry.domain.Name
import pl.slyberry.login.R
import pl.slyberry.login.gender.view.GenderFragmentViewEntity
import pl.slyberry.login.gender.view.GenderViewEvent
import pl.slyberry.login.gender.view.NavigatedFromGenderViewEvent
import pl.slyberry.login.gender.view.SubmitGenderViewEvent
import pl.slyberry.ui.android.Image
import pl.slyberry.ui.view.picker.GenderPickerItemViewEntity
import pl.slyberry.ui.view.picker.GenderPickerViewEntity
import pl.slyberry.ui.view.picker.SelectedSide

internal fun GenderModel.toViewEntity() = name?.let {
  GenderFragmentViewEntityFactory.createViewEntity(it)
}

internal object GenderFragmentViewEntityFactory {
  internal fun createViewEntity(name: Name): GenderFragmentViewEntity {
    return GenderFragmentViewEntity(
      name.value,
      GenderPickerViewEntity(createWomanViewEntity(), createManViewEntity(), null)
    )
  }

  private fun createWomanViewEntity(): GenderPickerItemViewEntity {
    return GenderPickerItemViewEntity(
      Image.create(R.drawable.radio_gender_woman_disabled),
      Image.create(R.drawable.radio_gender_woman_active)
    )
  }

  private fun createManViewEntity(): GenderPickerItemViewEntity {
    return GenderPickerItemViewEntity(
      Image.create(R.drawable.radio_gender_man_disabled),
      Image.create(R.drawable.radio_gender_man_active)
    )
  }
}

internal fun GenderViewEvent.toAction(): Action<GenderModel> {
  return when (this) {
    is SubmitGenderViewEvent -> this.toAction()
    NavigatedFromGenderViewEvent -> NavigatedFromGenderAction()
  }
}

private fun SubmitGenderViewEvent.toAction() : Action<GenderModel> {
  val gender = when (selectedSide) {
    SelectedSide.LEFT -> Gender.WOMAN
    SelectedSide.RIGHT -> Gender.MAN
    else -> throw IllegalArgumentException("Gender cannot be null.")
  }
  return SelectGenderAction(gender, BirthDate(birthDate))
}
