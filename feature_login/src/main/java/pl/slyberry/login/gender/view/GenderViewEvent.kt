package pl.slyberry.login.gender.view

import pl.slyberry.domain.DateWrapper
import pl.slyberry.ui.view.picker.SelectedSide

sealed class GenderViewEvent

data class SubmitGenderViewEvent(val selectedSide: SelectedSide, val birthDate: DateWrapper) : GenderViewEvent()

object NavigatedFromGenderViewEvent : GenderViewEvent()
