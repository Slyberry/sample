package pl.slyberry.login.gender.domain

import pl.slyberry.android.common.usecase.Completed
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.BirthDate
import pl.slyberry.domain.Gender
import pl.slyberry.repository.Repository

data class GenderParam(val gender: Gender, val birthDate: BirthDate)

class SetGenderUseCase(private val repository: Repository) : DefaultUseCase<GenderParam, Completed>() {

  override suspend fun performJob(param: GenderParam): Completed {
    repository.saveGender(param.gender)
    repository.saveBirthdayDate(param.birthDate)
    return Completed
  }
}
