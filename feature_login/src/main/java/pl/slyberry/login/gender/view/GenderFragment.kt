package pl.slyberry.login.gender.view

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.exhaustive
import pl.slyberry.android.common.ext.makeVisible
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.android.common.viewbinding.viewBinding
import pl.slyberry.domain.DateWrapper
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.login.R
import pl.slyberry.login.databinding.GenderFragmentBinding
import pl.slyberry.login.gender.presentation.GenderModel
import pl.slyberry.login.gender.presentation.GenderViewModel
import pl.slyberry.login.gender.presentation.toViewEntity
import pl.slyberry.ui.font.FontSpan
import pl.slyberry.ui.view.picker.SelectedSide

internal class GenderFragment : ViewEventFragment<GenderViewEvent>(R.layout.gender_fragment) {

  override val viewModel by viewModel<GenderViewModel>()
  private val binding by viewBinding(GenderFragmentBinding::bind)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with(binding) {
      toolbar.buttonBack.setOnClickListener { findNavController().navigateUp() }
      buttonNext.setOnClickListener { createViewEvent()?.let { emitToChannel(it) } }

      viewModel.model.observeKt(viewLifecycleOwner) {
        it.toViewEntity()?.let {
          dataInfoText.text = getDataInfoText(it.name)
          genderView.applyViewEntity(it.genderViewEntity)
        }
        handleNavigation(it)
      }
    }
  }

  private fun getDataInfoText(name: String): SpannableString {
    return SpannableString(requireContext().getString(R.string.gender_s_hello_info, name)).apply {
      setSpan(
        FontSpan(ResourcesCompat.getFont(requireContext(), R.font.montserrat_bold)),
        3,
        3 + name.length + 1,
        Spannable.SPAN_INCLUSIVE_INCLUSIVE
      )
    }
  }

  private fun createViewEvent(): SubmitGenderViewEvent? {
    try {
      val date = DateWrapper.createFromYears(binding.birthdateData.text.toString().toLong())
      val selectedSide: SelectedSide = binding.genderView.viewEntity.selectedSide!!
      return SubmitGenderViewEvent(selectedSide, date)
    } catch (it: Throwable) {
      it.printStackTrace()
      binding.errorInfo.makeVisible()
    }
    return null
  }

  private fun handleNavigation(model: GenderModel) {
    when (model.navigation) {
      Navigation.NEXT -> {
        findNavController().navigate(GenderFragmentDirections.navigateToBodyParamsFragment())
        emitToChannel(NavigatedFromGenderViewEvent)
      }
      Navigation.BACK -> navigateUp()
      Navigation.CURRENT -> Unit
    }.exhaustive
  }
}
