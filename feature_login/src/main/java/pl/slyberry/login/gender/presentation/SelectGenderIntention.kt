package pl.slyberry.login.gender.presentation

import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.Name
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status

class GetNameLoadingIntention : Intention<GenderModel> {

  override fun reduce(oldState: GenderModel): GenderModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class GetNameSuccessIntention(val name: Name) : Intention<GenderModel> {

  override fun reduce(oldState: GenderModel): GenderModel {
    return oldState.copy(name = name, status = Status.IDLE)
  }
}

class GetNameErrorIntention : Intention<GenderModel> {

  override fun reduce(oldState: GenderModel): GenderModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class SetGenderLoadingIntention : Intention<GenderModel> {

  override fun reduce(oldState: GenderModel): GenderModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class SetGenderSuccessIntention : Intention<GenderModel> {

  override fun reduce(oldState: GenderModel): GenderModel {
    return oldState.copy(status = Status.IDLE, navigation = Navigation.NEXT)
  }
}

class SetGenderErrorIntention : Intention<GenderModel> {

  override fun reduce(oldState: GenderModel): GenderModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class NavigatedFromGenderIntention : Intention<GenderModel> {

  override fun reduce(oldState: GenderModel): GenderModel {
    return oldState.copy(navigation = Navigation.CURRENT)
  }
}
