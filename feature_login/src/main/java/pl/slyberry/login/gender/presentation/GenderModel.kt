package pl.slyberry.login.gender.presentation

import pl.slyberry.domain.Name
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status

data class GenderModel(val name: Name?, val status: Status, val navigation: Navigation) {

  companion object {
    fun default() = GenderModel(null, Status.IDLE, Navigation.CURRENT)
  }
}
