package pl.slyberry.login.gender.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention

class NavigatedFromGenderAction : Action<GenderModel> {

  override fun perform(): Flow<Intention<GenderModel>> {
    return flow { emit(NavigatedFromGenderIntention()) }
  }
}
