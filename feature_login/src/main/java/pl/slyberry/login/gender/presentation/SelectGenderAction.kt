package pl.slyberry.login.gender.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.domain.BirthDate
import pl.slyberry.domain.Gender
import pl.slyberry.login.gender.domain.GenderParam
import pl.slyberry.login.gender.domain.SetGenderUseCase

class SelectGenderAction(private val gender: Gender, private val birthDate: BirthDate) : Action<GenderModel>, KoinComponent {

  private val useCase by inject<SetGenderUseCase>()

  override fun perform(): Flow<Intention<GenderModel>> {
    return useCase.perform(GenderParam(gender, birthDate)).map {
      when (it) {
        is LoadingResult -> SetGenderLoadingIntention()
        is SuccessResult -> SetGenderSuccessIntention()
        is ErrorResult -> SetGenderErrorIntention()
      }
    }
  }
}
