package pl.slyberry.login.gender.domain

import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.Name
import pl.slyberry.repository.Repository

class GetNameUseCase(private val repository: Repository) : DefaultUseCase<Unit, Name>() {

  override suspend fun performJob(param: Unit): Name {
    return repository.getName()
  }
}
