package pl.slyberry.login.gender.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.login.gender.domain.GetNameUseCase

class GetNameAction : Action<GenderModel>, KoinComponent {

  private val useCase by inject<GetNameUseCase>()

  override fun perform(): Flow<Intention<GenderModel>> {
    return useCase.perform(Unit).map {
      when (it) {
        is LoadingResult -> GetNameLoadingIntention()
        is SuccessResult -> GetNameSuccessIntention(it.data)
        is ErrorResult -> GetNameErrorIntention()
      }
    }
  }
}
