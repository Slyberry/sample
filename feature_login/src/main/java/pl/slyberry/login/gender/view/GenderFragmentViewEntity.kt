package pl.slyberry.login.gender.view

import pl.slyberry.ui.view.picker.GenderPickerViewEntity

class GenderFragmentViewEntity(val name: String, val genderViewEntity: GenderPickerViewEntity)
