package pl.slyberry.login.goal.presentation

import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status

class ShowGetBodyParamsLoadingIntention : Intention<GoalsModel> {

  override fun reduce(oldState: GoalsModel): GoalsModel {
    return oldState.copy(status = Status.IDLE)
  }
}

class ShowGetBodyParamsSuccessIntentionLoader(val weightAndHeight: WeightAndHeightStatistics) : Intention<GoalsModel> {

  override fun reduce(oldState: GoalsModel): GoalsModel {
    return oldState.copy(weightAndHeight = weightAndHeight, status = Status.SYNCING)
  }
}

class ShowGetBodyParamsErrorIntention : Intention<GoalsModel> {

  override fun reduce(oldState: GoalsModel): GoalsModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class ShowSetGoalLoadingIntention : Intention<GoalsModel> {

  override fun reduce(oldState: GoalsModel): GoalsModel {
    return oldState.copy(status = Status.IDLE)
  }
}

class ShowSetGoalSuccessIntentionLoader : Intention<GoalsModel> {

  override fun reduce(oldState: GoalsModel): GoalsModel {
    return oldState.copy(status = Status.SYNCING, navigation = Navigation.NEXT)
  }
}

class ShowSetGoalErrorIntention : Intention<GoalsModel> {

  override fun reduce(oldState: GoalsModel): GoalsModel {
    return oldState.copy(status = Status.ERROR)
  }
}
