package pl.slyberry.login.goal.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.domain.GoalCreationData
import pl.slyberry.domain.Weight
import pl.slyberry.domain.WeightGoal
import pl.slyberry.domain.WeightGoalType
import pl.slyberry.login.goal.domain.SetGoalUseCase

class SetGoalAction(val param: GoalCreationData) : Action<GoalsModel>, KoinComponent {

  private val useCase by inject<SetGoalUseCase>()

  override fun perform(): Flow<Intention<GoalsModel>> {
    return useCase.perform(param).map {
      when (it) {
        is LoadingResult -> ShowSetGoalLoadingIntention()
        is SuccessResult -> ShowSetGoalSuccessIntentionLoader()
        is ErrorResult -> ShowSetGoalErrorIntention()
      }
    }
  }
}
