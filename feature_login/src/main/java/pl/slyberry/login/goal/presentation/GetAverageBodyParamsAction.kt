package pl.slyberry.login.goal.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.repository.usecase.GetAverageBodyParamsUseCase

class GetAverageBodyParamsAction : Action<GoalsModel>, KoinComponent {

  private val getAverageBodyParamsUseCase by inject<GetAverageBodyParamsUseCase>()

  override fun perform(): Flow<Intention<GoalsModel>> {
    return getAverageBodyParamsUseCase.perform(Unit).map {
      when (it) {
        is LoadingResult -> ShowGetBodyParamsLoadingIntention()
        is SuccessResult -> ShowGetBodyParamsSuccessIntentionLoader(it.data)
        is ErrorResult -> ShowGetBodyParamsErrorIntention()
      }
    }
  }
}
