package pl.slyberry.login.goal.view

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.exhaustive
import pl.slyberry.android.common.ext.makeGone
import pl.slyberry.android.common.ext.makeVisible
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.login.R
import pl.slyberry.login.databinding.GoalsFragmentBinding
import pl.slyberry.login.goal.presentation.GoalsModel
import pl.slyberry.login.goal.presentation.GoalsViewModel
import pl.slyberry.ui.view.ruler.weight.WeightViewEntity

internal class GoalsFragment : ViewEventFragment<SetGoalViewEvent>(R.layout.goals_fragment) {

  override val viewModel by viewModel<GoalsViewModel>()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with(GoalsFragmentBinding.bind(view)) {
      toolbar.buttonBack.setOnClickListener { findNavController().navigateUp() }
      viewModel.model.observeKt(viewLifecycleOwner) {
        handleNavigation(it)

        (it.weightAndHeight)?.let {
          weightRuler.applyViewEntity(WeightViewEntity(it.averageWeight.value.toInt(), it.maxWeight.value.toInt()))
        }
      }

      buttonNext.setOnClickListener {
        val goal = when (goalsRadioGroup.checkedRadioButtonId) {
          R.id.goal_loss_button -> SelectedGoal.LOSS
          R.id.goal_keep_button -> SelectedGoal.KEEP
          R.id.goal_gain_button -> SelectedGoal.GAIN
          else -> null
        }
        goal?.let {
          emitToChannel(SetGoalViewEvent(it, weightRuler.selectedValue))
        } ?: errorInfo.makeVisible()
      }

      goalsRadioGroup.setOnCheckedChangeListener { _, checkedId ->
        when (checkedId) {
          R.id.goal_loss_button -> weightRuler.makeVisible()
          R.id.goal_keep_button -> weightRuler.makeGone()
          R.id.goal_gain_button -> weightRuler.makeVisible()
        }
      }
    }
  }

  private fun handleNavigation(model: GoalsModel) {
    when (model.navigation) {
      Navigation.NEXT -> {
        findNavController().navigate(GoalsFragmentDirections.actionGoalsFragmentPop())
      }
      Navigation.BACK -> navigateUp()
      Navigation.CURRENT -> Unit
    }.exhaustive
  }
}
