package pl.slyberry.login.goal.presentation

import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status

data class GoalsModel(val weightAndHeight: WeightAndHeightStatistics?, val status: Status, val navigation: Navigation) {

  companion object {
    fun default() = GoalsModel(null, Status.IDLE, Navigation.CURRENT)
  }
}
