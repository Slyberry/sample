package pl.slyberry.login.goal.domain

import pl.slyberry.android.common.usecase.Completed
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.GoalCreationData
import pl.slyberry.domain.WeightGoal
import pl.slyberry.repository.Repository

class SetGoalUseCase(private val repository: Repository) : DefaultUseCase<GoalCreationData, Completed>() {

  override suspend fun performJob(param: GoalCreationData): Completed {
    val weight = repository.getRecentWeightParams()
    val goal = WeightGoal(weight.current, param.target, param.type)
    repository.saveGoal(goal)
    repository.setIsInitialized()
    return Completed
  }
}
