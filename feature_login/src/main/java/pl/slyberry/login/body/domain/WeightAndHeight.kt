package pl.slyberry.login.body.domain

import pl.slyberry.domain.Height
import pl.slyberry.domain.Weight

data class WeightAndHeight(val weight: Weight, val height: Height)
