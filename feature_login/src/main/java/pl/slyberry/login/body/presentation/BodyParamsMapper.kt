package pl.slyberry.login.body.presentation

import pl.slyberry.android.common.mvi.Action
import pl.slyberry.domain.Height
import pl.slyberry.domain.Weight
import pl.slyberry.login.body.view.BodyParamsViewEvent
import pl.slyberry.login.body.view.NavigatedFromBodyParamsViewEvent
import pl.slyberry.login.body.view.SubmitBodyParamsViewEvent

fun BodyParamsViewEvent.toAction(): Action<BodyParamsModel> {
  return when (this) {
    is SubmitBodyParamsViewEvent -> SetBodyParamsAction(Height(height), Weight(weight))
    NavigatedFromBodyParamsViewEvent -> NavigatedFromBodyParamsAction()
  }
}
