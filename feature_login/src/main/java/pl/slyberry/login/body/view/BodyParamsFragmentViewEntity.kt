package pl.slyberry.login.body.view

import pl.slyberry.ui.android.Label

internal class BodyParamsFragmentViewEntity(
  val weightValueText: Label,
  val heightValueText: Label
)
