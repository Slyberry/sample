package pl.slyberry.login.body.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.domain.Height
import pl.slyberry.domain.Weight
import pl.slyberry.login.body.domain.SetBodyParamsUseCase
import pl.slyberry.login.body.domain.WeightAndHeight

internal class SetBodyParamsAction(private val height: Height, private val weight: Weight) : Action<BodyParamsModel>, KoinComponent {

  private val useCase by inject<SetBodyParamsUseCase>()

  override fun perform(): Flow<Intention<BodyParamsModel>> {
    return useCase.perform(WeightAndHeight(weight, height)).map {
      when (it) {
        is LoadingResult -> SetWeightAndHeightLoadingIntention()
        is SuccessResult -> SetWeightAndHeightSuccessIntention()
        is ErrorResult -> SetWeightAndHeightErrorIntention()
      }
    }
  }
}
