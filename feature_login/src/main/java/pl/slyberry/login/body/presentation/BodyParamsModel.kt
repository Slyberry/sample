package pl.slyberry.login.body.presentation

import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status

data class BodyParamsModel(val weightAndHeight: WeightAndHeightStatistics?, val status: Status, val navigation: Navigation) {

  companion object {
    fun default() = BodyParamsModel(null, Status.IDLE, Navigation.CURRENT)
  }
}
