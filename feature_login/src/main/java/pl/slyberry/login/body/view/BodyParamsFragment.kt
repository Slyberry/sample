package pl.slyberry.login.body.view

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.exhaustive
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.login.R
import pl.slyberry.login.body.presentation.BodyParamsModel
import pl.slyberry.login.body.presentation.BodyParamsViewModel
import pl.slyberry.login.body.presentation.toHeightViewEntity
import pl.slyberry.login.body.presentation.toWeightRulerViewEntity
import pl.slyberry.login.databinding.BodyParamsFragmentBinding

internal class BodyParamsFragment : ViewEventFragment<BodyParamsViewEvent>(R.layout.body_params_fragment) {

  override val viewModel by viewModel<BodyParamsViewModel>()

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    with(BodyParamsFragmentBinding.bind(view)) {
      viewModel.state.observeKt(viewLifecycleOwner) {
        it.toHeightViewEntity()?.let { heightRuler.applyViewEntity(it) }
        it.toWeightRulerViewEntity()?.let { weightRuler.applyViewEntity(it) }
        handleNavigation(it)
      }

      toolbar.buttonBack.setOnClickListener { findNavController().navigateUp() }
      buttonNext.setOnClickListener { emitToChannel(SubmitBodyParamsViewEvent(heightRuler.selectedValue, weightRuler.selectedValue)) }
    }
  }

  private fun handleNavigation(model: BodyParamsModel) {
    when (model.navigation) {
      Navigation.NEXT -> {
        findNavController().navigate(BodyParamsFragmentDirections.navigateToGoalsFragment())
        emitToChannel(NavigatedFromBodyParamsViewEvent)
      }
      Navigation.BACK -> navigateUp()
      Navigation.CURRENT -> Unit
    }.exhaustive
  }
}
