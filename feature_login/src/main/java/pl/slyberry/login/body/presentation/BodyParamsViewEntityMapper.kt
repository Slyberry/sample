package pl.slyberry.login.body.presentation

import pl.slyberry.ui.view.ruler.height.HeightViewEntity
import pl.slyberry.ui.view.ruler.weight.WeightViewEntity

fun BodyParamsModel.toHeightViewEntity(): HeightViewEntity? {
  return weightAndHeight?.let {
    HeightViewEntity(it.averageHeight.value.toInt(), it.maxHeight.value.toInt())
  }
}

fun BodyParamsModel.toWeightRulerViewEntity(): WeightViewEntity? {
  return weightAndHeight?.let {
    WeightViewEntity(it.averageWeight.value.toInt(), it.maxWeight.value.toInt())
  }
}
