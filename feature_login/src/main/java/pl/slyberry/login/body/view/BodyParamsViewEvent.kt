package pl.slyberry.login.body.view

sealed class BodyParamsViewEvent

class SubmitBodyParamsViewEvent(val height: Int, val weight: Int) : BodyParamsViewEvent()

object NavigatedFromBodyParamsViewEvent : BodyParamsViewEvent()
