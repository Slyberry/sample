package pl.slyberry.login.body.presentation

import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status

class SetWeightAndHeightLoadingIntention : Intention<BodyParamsModel> {

  override fun reduce(oldState: BodyParamsModel): BodyParamsModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class SetWeightAndHeightSuccessIntention : Intention<BodyParamsModel> {

  override fun reduce(oldState: BodyParamsModel): BodyParamsModel {
    return oldState.copy(status = Status.IDLE, navigation = Navigation.NEXT)
  }
}

class SetWeightAndHeightErrorIntention : Intention<BodyParamsModel> {

  override fun reduce(oldState: BodyParamsModel): BodyParamsModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class GetWeightAndHeightLoadingIntention : Intention<BodyParamsModel> {

  override fun reduce(oldState: BodyParamsModel): BodyParamsModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class GetWeightAndHeightSuccessIntention(val weightAndHeight: WeightAndHeightStatistics) : Intention<BodyParamsModel> {

  override fun reduce(oldState: BodyParamsModel): BodyParamsModel {
    return oldState.copy(weightAndHeight = weightAndHeight, status = Status.IDLE)
  }
}

class GetWeightAndHeightErrorIntention : Intention<BodyParamsModel> {

  override fun reduce(oldState: BodyParamsModel): BodyParamsModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class NavigatedFromBodyParamsIntention : Intention<BodyParamsModel> {

  override fun reduce(oldState: BodyParamsModel): BodyParamsModel {
    return oldState.copy(navigation = Navigation.CURRENT)
  }
}
