package pl.slyberry.login.body.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer
import pl.slyberry.login.body.view.BodyParamsViewEvent

internal class BodyParamsViewModel : ViewModel(), ViewEventConsumer<BodyParamsViewEvent> {

  val state = MutableLiveData<BodyParamsModel>()

  private val modelStore = ModelStore(viewModelScope, BodyParamsModel.default())

  private val eventsChannel = Channel<BodyParamsViewEvent>()

  init {
    modelStore.modelState()
      .onEach {
        state.value = it
      }
      .launchIn(viewModelScope)

    eventsChannel.consumeAsFlow().onEach {
      lunchAction(it.toAction())
    }.launchIn(viewModelScope)

    lunchAction(GetAverageBodyParamsAction())
  }

  override fun onViewEvent(event: BodyParamsViewEvent) {
    eventsChannel.offer(event)
  }

  private fun lunchAction(action: Action<BodyParamsModel>) {
    action.perform().onEach { modelStore.process(it) }.launchIn(viewModelScope)
  }
}
