package pl.slyberry.login.body.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention

class NavigatedFromBodyParamsAction : Action<BodyParamsModel> {

  override fun perform(): Flow<Intention<BodyParamsModel>> {
    return flow { emit(NavigatedFromBodyParamsIntention()) }
  }
}
