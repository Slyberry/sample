package pl.slyberry.login.body.domain

import pl.slyberry.android.common.usecase.Completed
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.repository.Repository

class SetBodyParamsUseCase(private val repository: Repository) : DefaultUseCase<WeightAndHeight, Completed>() {

  override suspend fun performJob(param: WeightAndHeight): Completed {
    repository.saveHeight(param.height)
    repository.saveWeight(param.weight)
    return Completed
  }
}
