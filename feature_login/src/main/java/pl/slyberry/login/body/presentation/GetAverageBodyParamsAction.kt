package pl.slyberry.login.body.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.repository.usecase.GetAverageBodyParamsUseCase

class GetAverageBodyParamsAction : Action<BodyParamsModel>, KoinComponent {

  private val getAverageBodyParamsUseCase by inject<GetAverageBodyParamsUseCase>()

  override fun perform(): Flow<Intention<BodyParamsModel>> {
    return getAverageBodyParamsUseCase.perform(Unit).map {
      when (it) {
        is LoadingResult -> GetWeightAndHeightLoadingIntention()
        is SuccessResult -> GetWeightAndHeightSuccessIntention(it.data)
        is ErrorResult -> GetWeightAndHeightErrorIntention()
      }
    }
  }
}
