package pl.slyberry.login.body.view

import pl.slyberry.ui.view.ruler.height.HeightViewEntity
import pl.slyberry.ui.view.ruler.weight.WeightViewEntity

data class BodyParamsInputInitViewEntity(val heightViewEntity: HeightViewEntity, val weightViewEntity: WeightViewEntity)
