package pl.slyberry.login

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pl.slyberry.login.body.domain.SetBodyParamsUseCase
import pl.slyberry.login.body.presentation.BodyParamsViewModel
import pl.slyberry.repository.usecase.GetAverageBodyParamsUseCase
import pl.slyberry.login.gender.domain.GetNameUseCase
import pl.slyberry.login.gender.domain.SetGenderUseCase
import pl.slyberry.login.gender.presentation.GenderViewModel
import pl.slyberry.login.goal.domain.SetGoalUseCase
import pl.slyberry.login.goal.presentation.GoalsViewModel
import pl.slyberry.login.main.domain.SaveNameUseCase
import pl.slyberry.login.main.presentation.LoginViewModel

val featureLoginModule = module {
  viewModel { BodyParamsViewModel() }
  viewModel { GenderViewModel() }
  viewModel { GoalsViewModel() }
  viewModel { LoginViewModel() }

  factory { SaveNameUseCase(get()) }
  factory { GetNameUseCase(get()) }
  factory { SetGenderUseCase(get()) }
  factory { SetBodyParamsUseCase(get()) }
  factory { SetGoalUseCase(get()) }
}
