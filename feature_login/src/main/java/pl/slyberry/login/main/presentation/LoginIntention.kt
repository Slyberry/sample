package pl.slyberry.login.main.presentation

import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.mvi.GenericModel
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status

class ShowStoreNameLoadingIntention : Intention<GenericModel> {

  override fun reduce(oldState: GenericModel): GenericModel {
    return oldState.copy(status = Status.SYNCING)
  }
}

class ShowStoreNameSuccessIntention : Intention<GenericModel> {

  override fun reduce(oldState: GenericModel): GenericModel {
    return oldState.copy(status = Status.IDLE, navigation = Navigation.NEXT)
  }
}

class ShowStoreNameErrorIntention : Intention<GenericModel> {

  override fun reduce(oldState: GenericModel): GenericModel {
    return oldState.copy(status = Status.ERROR)
  }
}

class NavigatedFromLoginIntention : Intention<GenericModel> {

  override fun reduce(oldState: GenericModel): GenericModel {
    return oldState.copy(navigation = Navigation.CURRENT)
  }
}
