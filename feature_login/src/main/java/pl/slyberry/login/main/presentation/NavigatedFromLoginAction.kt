package pl.slyberry.login.main.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.domain.mvi.GenericModel

class NavigatedFromLoginAction : Action<GenericModel> {

  override fun perform(): Flow<Intention<GenericModel>> {
    return flow { emit(NavigatedFromLoginIntention()) }
  }
}
