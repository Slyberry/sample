package pl.slyberry.login.main.presentation

import pl.slyberry.android.common.mvi.Action
import pl.slyberry.domain.Name
import pl.slyberry.domain.mvi.GenericModel
import pl.slyberry.login.main.view.LoginViewEvent
import pl.slyberry.login.main.view.NavigatedNextViewEvent
import pl.slyberry.login.main.view.SubmitNameViewEvent

fun LoginViewEvent.toAction(): Action<GenericModel> {
  return when (this) {
    is SubmitNameViewEvent -> StoreNameAction(Name(name))
    NavigatedNextViewEvent -> NavigatedFromLoginAction()
  }
}
