package pl.slyberry.login.main.view

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import pl.slyberry.android.common.ext.exhaustive
import pl.slyberry.android.common.ext.observeKt
import pl.slyberry.android.common.mvi.ViewEventFragment
import pl.slyberry.android.common.viewbinding.viewBinding
import pl.slyberry.domain.mvi.GenericModel
import pl.slyberry.domain.mvi.Navigation
import pl.slyberry.domain.mvi.Status
import pl.slyberry.login.R
import pl.slyberry.login.databinding.LoginMainFragmentBinding
import pl.slyberry.login.main.presentation.LoginViewModel

internal class LoginFragment : ViewEventFragment<LoginViewEvent>(R.layout.login_main_fragment) {

  override val viewModel by viewModel<LoginViewModel>()

  private val binding by viewBinding(LoginMainFragmentBinding::bind)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    viewModel.state.observeKt(viewLifecycleOwner) {
      handleNavigation(it)
      if (it.status == Status.ERROR) {
        binding.inputText.error = requireContext().getString(R.string.login_s_error_cannot_be_empty)
      }
    }

//    binding.inputTextLayout.typeface = Typeface.createFromAsset(requireContext().assets, "lib_ui/font/montserrat_medium.ttf")

    binding.let { binding ->
      binding.buttonNext.setOnClickListener { emitToChannel(SubmitNameViewEvent(getName(binding))) }
    }
  }

  private fun handleNavigation(model: GenericModel) {
    when (model.navigation) {
      Navigation.NEXT -> {
        findNavController().navigate(LoginFragmentDirections.navigateToGenderFragment())
        emitToChannel(NavigatedNextViewEvent)
      }
      Navigation.BACK -> navigateUp()
      Navigation.CURRENT -> Unit
    }.exhaustive
  }

  private fun getName(binding: LoginMainFragmentBinding) = binding.inputText.text.toString()
}
