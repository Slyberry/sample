package pl.slyberry.login.main.view

sealed class LoginViewEvent

data class SubmitNameViewEvent(val name: String) : LoginViewEvent()

object NavigatedNextViewEvent : LoginViewEvent()
