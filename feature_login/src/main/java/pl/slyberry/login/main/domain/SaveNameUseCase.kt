package pl.slyberry.login.main.domain

import pl.slyberry.android.common.usecase.Completed
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.Name
import pl.slyberry.repository.Repository

class SaveNameUseCase(private val repository: Repository) : DefaultUseCase<Name, Completed>() {

  override suspend fun performJob(param: Name): Completed {
    repository.saveName(param)
    return Completed
  }
}
