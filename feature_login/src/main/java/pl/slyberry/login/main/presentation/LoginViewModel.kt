package pl.slyberry.login.main.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import pl.slyberry.android.common.mvi.ModelStore
import pl.slyberry.android.common.mvi.ViewEventConsumer
import pl.slyberry.domain.mvi.GenericModel
import pl.slyberry.login.main.view.LoginViewEvent

internal class LoginViewModel() : ViewModel(), ViewEventConsumer<LoginViewEvent> {

  val state = MutableLiveData<GenericModel>()

  private val modelStore = ModelStore(viewModelScope, GenericModel.default())

  private val eventsChannel = Channel<LoginViewEvent>()

  init {
    modelStore.modelState()
      .onEach {
        state.value = it
      }.launchIn(viewModelScope)

    eventsChannel.consumeAsFlow().onEach {
      it.toAction().perform().onEach { modelStore.process(it) }.launchIn(viewModelScope)
    }.launchIn(viewModelScope)
  }

  override fun onViewEvent(event: LoginViewEvent) {
    eventsChannel.offer(event)
  }
}
