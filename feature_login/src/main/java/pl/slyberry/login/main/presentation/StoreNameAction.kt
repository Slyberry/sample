package pl.slyberry.login.main.presentation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import org.koin.core.KoinComponent
import org.koin.core.inject
import pl.slyberry.android.common.mvi.Action
import pl.slyberry.android.common.mvi.Intention
import pl.slyberry.android.common.usecase.ErrorResult
import pl.slyberry.android.common.usecase.LoadingResult
import pl.slyberry.android.common.usecase.SuccessResult
import pl.slyberry.domain.Name
import pl.slyberry.domain.mvi.GenericModel
import pl.slyberry.login.main.domain.SaveNameUseCase

class StoreNameAction(val name: Name) : Action<GenericModel>, KoinComponent {

  private val saveNameUseCase by inject<SaveNameUseCase>()

  override fun perform(): Flow<Intention<GenericModel>> {
    if (!name.isValid()) {
      return flow { emit(ShowStoreNameErrorIntention()) }
    }
    return saveNameUseCase.perform(name).map {
      when (it) {
        is LoadingResult -> ShowStoreNameLoadingIntention()
        is SuccessResult -> ShowStoreNameSuccessIntention()
        is ErrorResult -> ShowStoreNameErrorIntention()
      }
    }
  }
}
