package pl.slyberry.login

import org.junit.Test
import pl.slyberry.domain.DateWrapper

class DateWrapperTest {

  @Test
  fun `when parsing date should return DateWrapper object`() {
    val date = "2020-07-07"

    val dateWrapper = DateWrapper.create(date)

    assert(date == dateWrapper.toDisplayString())
  }
}
