rootProject.buildFileName = "build.gradle.kts"

include(":app")
include(":feature_measure")
include(":feature_login")
include(":lib_common")
include(":lib_db")
include(":lib_domain")
include(":lib_repository")
include(":lib_ui")
