package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BodyParamsInitData(
        val birthDate: BirthDate,
        val weight: Weight,
        val height: Height
) : Parcelable
