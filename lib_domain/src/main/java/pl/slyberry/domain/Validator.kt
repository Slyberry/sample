package pl.slyberry.domain

interface Validator<T> {

  fun isValid(value: T): Boolean
}
