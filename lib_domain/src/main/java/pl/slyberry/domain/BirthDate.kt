package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BirthDate(val value: DateWrapper) : Parcelable
