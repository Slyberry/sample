package pl.slyberry.domain

object MeasuredConstants {

  object Height {
    val AVERAGE = Height(167f)
    val MAX = Height(255f)
  }

  object Weight {
    val AVERAGE = Weight(62f)
    val MAX = Weight(255f)
  }
}
