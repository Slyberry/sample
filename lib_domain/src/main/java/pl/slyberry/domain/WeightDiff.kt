package pl.slyberry.domain

data class WeightDiff(val value: Float)
