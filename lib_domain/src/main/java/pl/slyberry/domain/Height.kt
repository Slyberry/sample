package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Height(val value: Float) : Parcelable {

  constructor(value: Int) : this(value.toFloat())
}
