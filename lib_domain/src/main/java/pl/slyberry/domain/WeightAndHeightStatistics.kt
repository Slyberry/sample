package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeightAndHeightStatistics(
  val averageWeight: Weight,
  val maxWeight: Weight,
  val averageHeight: Height,
  val maxHeight: Height
) : Parcelable
