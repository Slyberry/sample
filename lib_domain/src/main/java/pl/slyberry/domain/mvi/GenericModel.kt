package pl.slyberry.domain.mvi

data class GenericModel(val status: Status, val navigation: Navigation) {

  companion object {
    fun default() = GenericModel(Status.IDLE, Navigation.CURRENT)
  }
}

enum class Navigation {
  NEXT, BACK, CURRENT
}

enum class BackNavigation {
  CURRENT, BACK
}

enum class Status {
  IDLE, SYNCING, ERROR
}
