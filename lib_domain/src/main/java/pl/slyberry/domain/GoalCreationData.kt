package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GoalCreationData(val target: Weight, val type: WeightGoalType) : Parcelable
