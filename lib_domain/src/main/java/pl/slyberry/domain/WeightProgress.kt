package pl.slyberry.domain

data class WeightProgress(
  val start: Weight,
  val current: Weight,
  val target: Weight,
  val weightGoalType: WeightGoalType
)
