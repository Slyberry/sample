package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Weight(val value: Float, val id: Long = -1L) : Parcelable {

  constructor(value: Int): this(value.toFloat())
}

@Parcelize
data class WeightParams(val current: Weight, val max: Weight) : Parcelable
