package pl.slyberry.domain

class WeightHistoryItem(val weight: Weight, val date: DateWrapper, val change: WeightDiff, val id: Long)

class HeightHistoryItem(val height: Height, val date: DateWrapper, val change: WeightDiff, val id: Long)
