package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserInitData(
    val name: Name,
    val gender: Gender,
    val bodyParamsInitData: BodyParamsInitData,
    val goal: WeightGoal
) : Parcelable
