package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

private val formatter = DateTimeFormatter.ofPattern("dd/MM/uuuu")

@Parcelize
data class DateWrapper(val date: OffsetDateTime) : Parcelable, Comparable<DateWrapper> {

  fun toDisplayString() = date.format(formatter)

  fun serialize() = date.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)

  companion object {
    fun create(value: Long) = DateWrapper(OffsetDateTime.ofInstant(Instant.ofEpochMilli(value), ZoneId.systemDefault()))
    fun create(value: String) = DateWrapper(LocalDate.parse(value).atStartOfDay(ZoneId.systemDefault()).toOffsetDateTime())
    fun createFromYears(value: Long) = DateWrapper(OffsetDateTime.now().minusYears(value))
    fun nowSeconds() = OffsetDateTime.now().toEpochSecond()
    fun now() = DateWrapper(OffsetDateTime.now())
    fun deserialize(value: String) = DateWrapper(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(value, OffsetDateTime::from))
  }

  override fun compareTo(other: DateWrapper): Int {
    return date.compareTo(other.date)
  }
}
