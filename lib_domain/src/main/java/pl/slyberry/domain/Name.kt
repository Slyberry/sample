package pl.slyberry.domain

import android.os.Parcelable
import android.text.TextUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Name(val value: String) : Parcelable {

  fun isValid() = !TextUtils.isEmpty(value)
}
