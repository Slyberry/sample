package pl.slyberry.domain

import java.lang.IllegalArgumentException
import kotlin.reflect.KClass

class EnumInitializationException(message: String) : IllegalArgumentException() {

  constructor(kclass: KClass<*>, value: String) : this("Cannot create ${kclass.java.canonicalName} for provided value: $value")
}
