package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

enum class Gender(val value: String) {
  MAN("MAN"), WOMAN("WOMAN");

  companion object {
    fun create(value: String): Gender {
      return values().find { it.value == value } ?: throw EnumInitializationException(Gender::class, value)
    }
  }
}
