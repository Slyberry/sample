package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeightGoal(val start: Weight, val target: Weight, val type: WeightGoalType) : Parcelable

enum class WeightGoalType {
  LOSS, KEEP, GAIN
}
