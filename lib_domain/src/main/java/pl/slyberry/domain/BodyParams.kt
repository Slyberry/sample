package pl.slyberry.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BodyParams(
    val id: Long,
    val weight: Weight,
    val height: Height,
    val goal: WeightGoal,
    val creationDate: DateWrapper
) : Parcelable
