import pl.slyberry.buildsrc.Dependencies
import pl.slyberry.buildsrc.Libraries.implementNavigation

plugins {
    id("com.android.library")
    id("shared-gradle-plugin")
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementNavigation()

    implementation(Dependencies.LIVE_DATA)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.LIFECYCLE)
    implementation(Dependencies.LIFECYCLE_COMMON)
    implementation(Dependencies.LIFECYCLE_SCOPE)
    implementation(Dependencies.FLOW_BINDINGS)
}
