package pl.slyberry.android.common.mvi

import kotlinx.coroutines.flow.Flow

interface Action<T> {

  fun perform(): Flow<Intention<T>>
}
