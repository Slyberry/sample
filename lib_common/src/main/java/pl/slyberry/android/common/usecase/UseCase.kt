package pl.slyberry.android.common.usecase

import kotlinx.coroutines.flow.Flow

interface UseCase<Param, Data> {

  fun perform(param: Param): Flow<Result<Data>>
}
