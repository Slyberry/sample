package pl.slyberry.android.common.mvi

import kotlinx.coroutines.flow.Flow

interface ViewEventProducer<Event> {

}

interface ViewEventConsumer<Event> {

  fun onViewEvent(event: Event)
}
