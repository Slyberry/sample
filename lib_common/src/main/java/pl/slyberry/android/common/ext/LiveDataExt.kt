package pl.slyberry.android.common.ext

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

//FIXME some library mistake. This fun is already implemented.
fun <T> LiveData<T>.observeKt(lifecycleOwner: LifecycleOwner, block: (T) -> Unit) {
  observe(lifecycleOwner, Observer { block(it) })
}

fun <T> MutableLiveData<T>.setIfNotNull(newValue: T) {
  newValue?.let {
    value = it
  }
}
