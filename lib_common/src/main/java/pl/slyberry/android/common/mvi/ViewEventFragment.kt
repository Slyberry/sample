package pl.slyberry.android.common.mvi

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

abstract class ViewEventFragment<T>(@LayoutRes contentLayoutId: Int) : Fragment(contentLayoutId), ViewEventProducer<T> {

  protected abstract val viewModel: ViewEventConsumer<T> //work on it

  fun navigateUp() {
    findNavController().navigateUp()
  }

  fun emitToChannel(event: T) {
    viewModel.onViewEvent(event)
  }
}
