package pl.slyberry.android.common.usecase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

abstract class DefaultUseCase<Input, Output> : UseCase<Input, Output> {

  final override fun perform(param: Input): Flow<Result<Output>> {
    return flow {
      emit(Result.createLoading())
      emit(withContext(Dispatchers.IO) { Result.createSuccess(performJob(param)) })
    }.catch {
      it.printStackTrace()
      emit(Result.createError<Output>(it))
    }
  }

  protected abstract suspend fun performJob(param: Input): Output
}
