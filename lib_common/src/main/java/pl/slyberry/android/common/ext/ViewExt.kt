package pl.slyberry.android.common.ext

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver

fun View.layoutInflater() = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

inline fun View.waitForLayout(crossinline block: () -> Unit) = with(viewTreeObserver) {
  addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
    override fun onGlobalLayout() {
      removeOnGlobalLayoutListener(this)
      block()
//      postDelayed( { block() }, 2000)
    }
  })
}

fun View.makeVisible() {
  visibility = View.VISIBLE
}

fun View.makeInvisible() {
  visibility = View.INVISIBLE
}

fun View.makeGone() {
  visibility = View.GONE
}
