package pl.slyberry.android.common.ext

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.linearLayoutManager(): LinearLayoutManager {
  return layoutManager as LinearLayoutManager
}

fun LinearLayoutManager.getMiddleItemPosition(): Long {
    val firstItem = findFirstCompletelyVisibleItemPosition().toLong()
    val lastItem = findLastCompletelyVisibleItemPosition().toLong()
    return (firstItem+lastItem)/2
}

fun RecyclerView.centerOnPosition(position: Int) {
  linearLayoutManager().findViewByPosition(linearLayoutManager().findFirstCompletelyVisibleItemPosition())?.width?.let { viewWidth ->
    val offset = (width.toFloat()/2 - viewWidth.toFloat()/2).toInt()
    linearLayoutManager().scrollToPositionWithOffset(position, offset)
  }
}
