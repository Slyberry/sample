package pl.slyberry.android.common.mvi

enum class NavigationDirection {
  BACK, NEXT
}
