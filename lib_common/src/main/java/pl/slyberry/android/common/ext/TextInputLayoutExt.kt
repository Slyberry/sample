package pl.slyberry.android.common.ext

import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.requireEditText(): TextInputEditText {
  return editText!! as TextInputEditText
}

fun TextInputEditText.currentText() = text.toString()
