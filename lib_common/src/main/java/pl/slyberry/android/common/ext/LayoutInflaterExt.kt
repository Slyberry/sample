package pl.slyberry.android.common.ext

import android.view.LayoutInflater
import android.view.ViewGroup

fun LayoutInflater.inflateNoAttach(resId: Int, container: ViewGroup?) = inflate(resId, container, false)
