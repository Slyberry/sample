package pl.slyberry.android.common.ext

import android.content.SharedPreferences

fun SharedPreferences.requirePositiveFloat(key: String): Float {
  val value = getFloat(key, -1f)
  if (value >= 0) {
    return value
  } else {
    throw NotInitializedPropertyException()
  }
}

fun SharedPreferences.saveFloat(key: String, value: Float) {
  return edit().putFloat(key, value).apply()
}

fun SharedPreferences.requireString(key: String): String {
  return requireNotNull(getString(key, null))
}

fun SharedPreferences.saveString(key: String, value: String) {
  return edit().putString(key, value).apply()
}

fun SharedPreferences.requireBoolean(key: String): Boolean {
  return getBoolean(key, false)
}

fun SharedPreferences.saveBoolean(key: String, value: Boolean) {
  return edit().putBoolean(key, value).apply()
}

class NotInitializedPropertyException : Throwable("Property was't initialized.")
