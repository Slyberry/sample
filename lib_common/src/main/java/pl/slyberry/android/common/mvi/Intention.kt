package pl.slyberry.android.common.mvi

interface Intention<T> {
  fun reduce(oldState: T): T
}
