package pl.slyberry.android.common.ext

val <T> T.exhaustive: T
  get() = this
