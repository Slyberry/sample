import pl.slyberry.buildsrc.Dependencies
import pl.slyberry.buildsrc.Modules

plugins {
    id("com.android.library")
    id("shared-gradle-plugin")
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(project(Modules.LIB_COMMON))

    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.COORDINATOR)
    implementation(Dependencies.RECYCLER_VIEW_SNAP)
    implementation(Dependencies.COROUTINES)
    implementation(Dependencies.SWIPE_LAYOUT)

    testImplementation(Dependencies.Test.JUNIT)
}
