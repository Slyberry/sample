package pl.slyberry.ui.view.measure.history

import pl.slyberry.ui.R
import pl.slyberry.ui.android.Chroma
import pl.slyberry.ui.android.Label

data class WeightMeasureItemViewEntity(
  val id: Long,
  val date: Label,
  val weight: Label,
  val diff: Label,
  val background: WeightMeasureItemChroma
)

enum class WeightMeasureItemChroma(val value: Chroma) {
  ODD(Chroma.create(R.color.history_item_background_odd)),
  EVEN(Chroma.create(R.color.history_item_background_even))
}
