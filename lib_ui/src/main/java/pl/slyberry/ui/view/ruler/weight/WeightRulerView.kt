package pl.slyberry.ui.view.ruler.weight

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.consumeAsFlow
import pl.slyberry.android.common.ext.centerOnPosition
import pl.slyberry.android.common.ext.getMiddleItemPosition
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.android.common.ext.linearLayoutManager
import pl.slyberry.android.common.ext.waitForLayout
import pl.slyberry.ui.databinding.WeightRulerViewBinding

internal class WeightRulerView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = WeightRulerViewBinding.inflate(layoutInflater(), this, true)

  private val adapter = WeightRulerAdapter()

  private lateinit var viewEntity: WeightViewEntity

  fun applyViewEntity(newViewEntity: WeightViewEntity, onValueChange: (Int) -> Unit) {
    viewEntity = newViewEntity

    with(binding) {
      initAdapter(viewEntity.maxValue)

      recyclerView.waitForLayout {
        recyclerView.centerOnPosition(viewEntity.currentValue)
      }

      recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
          super.onScrollStateChanged(recyclerView, newState)
          if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            val currentValue = calculateValueUnderCurrentPosition()
            viewEntity = viewEntity.copy(currentValue = currentValue)
            onValueChange(currentValue)
          }
        }
      })
    }
  }

  private fun initAdapter(maxValue: Int) {
    if (binding.recyclerView.adapter == null) {
      adapter.setNewItemCount(maxValue)
      binding.recyclerView.adapter = adapter
    }
  }

  private fun calculateValueUnderCurrentPosition(): Int {
    return (binding.recyclerView.linearLayoutManager().getMiddleItemPosition() % viewEntity.maxValue).toInt()
  }
}
