package pl.slyberry.ui.view.ruler.weight

data class WeightViewEntity(val currentValue: Int, val maxValue: Int)
