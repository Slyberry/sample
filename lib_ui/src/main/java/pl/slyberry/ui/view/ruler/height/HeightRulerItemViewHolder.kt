package pl.slyberry.ui.view.ruler.height

import android.view.ViewGroup
import pl.slyberry.ui.R
import pl.slyberry.ui.android.setLabel
import pl.slyberry.ui.databinding.HeightRulerItemViewBinding
import pl.slyberry.ui.recyclerview.SimpleRecyclerViewHolder

internal class HeightRulerItemViewHolder(parent: ViewGroup) : SimpleRecyclerViewHolder(R.layout.height_ruler_item_view, parent) {

  fun applyViewEntity(viewEntity: HeightRulerItemViewEntity) {
    with (HeightRulerItemViewBinding.bind(itemView)) {
      measuredValueTitle.setLabel(viewEntity.amount)
    }
  }
}
