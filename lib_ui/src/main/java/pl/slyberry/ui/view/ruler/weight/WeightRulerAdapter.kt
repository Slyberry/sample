package pl.slyberry.ui.view.ruler.weight

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pl.slyberry.ui.android.Label
import pl.slyberry.ui.recyclerview.SimpleRecyclerViewHolder
import pl.slyberry.ui.view.ruler.height.HeightRulerDiffUtil
import pl.slyberry.ui.view.ruler.height.HeightRulerItemViewEntity

internal class WeightRulerAdapter : RecyclerView.Adapter<SimpleRecyclerViewHolder>() {

  private var items = emptyList<WeightRulerItemViewEntity>()

  fun setNewItemCount(count: Int) {
    val newItems = MutableList(count) { i -> WeightRulerItemViewEntity(Label.create(i.toString())) }
    val result = DiffUtil.calculateDiff(WeightRulerDiffUtil(items, newItems))
    items = newItems
    result.dispatchUpdatesTo(this)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleRecyclerViewHolder {
    return WeightRulerItemViewHolder(parent)
  }

  override fun getItemCount() = items.size

  override fun onBindViewHolder(holder: SimpleRecyclerViewHolder, position: Int) {
    (holder as WeightRulerItemViewHolder).applyViewEntity(items[position%items.size])
  }
}
