package pl.slyberry.ui.view.ruler.height

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.ui.R
import pl.slyberry.ui.databinding.HeightViewBinding

class HeightView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = HeightViewBinding.inflate(layoutInflater(), this, true)

  var selectedValue = 0
    private set

  init {
    context.obtainStyledAttributes(attrs, R.styleable.HeightView).also {
      binding.title.text = it.getString(R.styleable.HeightView_heightTitle)
      binding.unit.text = it.getString(R.styleable.HeightView_heightUnit)
    }.recycle()
  }

  fun applyViewEntity(newViewEntity: HeightViewEntity) {
    binding.measuredValue.text = newViewEntity.currentValue.toString()
    selectedValue = newViewEntity.currentValue
    binding.heightRuler.applyViewEntity(newViewEntity) {
      binding.measuredValue.text = it.toString()
      selectedValue = it
    }
  }
}
