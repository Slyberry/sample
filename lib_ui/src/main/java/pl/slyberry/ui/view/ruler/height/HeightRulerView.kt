package pl.slyberry.ui.view.ruler.height

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import pl.slyberry.android.common.ext.centerOnPosition
import pl.slyberry.android.common.ext.getMiddleItemPosition
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.android.common.ext.linearLayoutManager
import pl.slyberry.android.common.ext.waitForLayout
import pl.slyberry.ui.databinding.HeightRulerViewBinding

internal class HeightRulerView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = HeightRulerViewBinding.inflate(layoutInflater(), this, true)

  private val adapter = HeightRulerAdapter()

  private lateinit var viewEntity: HeightViewEntity

  fun applyViewEntity(newViewEntity: HeightViewEntity, onValueChange: (Int) -> Unit) {
    this.viewEntity = newViewEntity

    with(binding) {
      initAdapter(viewEntity.maxValue)

      recyclerView.waitForLayout {
        recyclerView.centerOnPosition(viewEntity.currentValue)
      }

      recyclerView.addOnScrollListener(object : OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
          super.onScrollStateChanged(recyclerView, newState)
          if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            val currentValue = calculateValueUnderCurrentPosition()
            viewEntity = viewEntity.copy(currentValue = currentValue)
            onValueChange(currentValue)
          }
        }
      })
    }
  }

  private fun initAdapter(maxValue: Int) {
    if (binding.recyclerView.adapter == null) {
      adapter.setNewItemCount(maxValue)
      binding.recyclerView.adapter = adapter
    }
  }

  private fun calculateValueUnderCurrentPosition(): Int {
    return (binding.recyclerView.linearLayoutManager().getMiddleItemPosition() % viewEntity.maxValue).toInt()
  }
}
