package pl.slyberry.ui.view.measure.history

import android.view.ViewGroup
import kotlinx.android.synthetic.main.weight_measure_item_view_holder.view.*
import pl.slyberry.ui.R
import pl.slyberry.ui.recyclerview.SimpleRecyclerViewHolder

class WeightMeasureItemViewHolder(parent: ViewGroup) : SimpleRecyclerViewHolder(R.layout.weight_measure_item_view_holder, parent) {

  fun applyViewEntity(viewEntity: WeightMeasureItemViewEntity, onClick: (Long) -> Unit) {
    with(itemView) {
      itemView.container.close(false)
      item.applyViewEntity(viewEntity)
      delete_item.setOnClickListener { onClick.invoke(viewEntity.id) }
    }
  }
}
