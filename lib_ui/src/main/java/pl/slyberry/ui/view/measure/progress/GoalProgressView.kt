package pl.slyberry.ui.view.measure.progress

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.ui.android.setLabel
import pl.slyberry.ui.databinding.GoalProgressViewBinding

class GoalProgressView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = GoalProgressViewBinding.inflate(layoutInflater(), this, true)

  fun applyViewEntity(viewEntity: GoalProgressViewEntity) {
    with (binding) {
      startValue.setLabel(viewEntity.displayStartValue)
      endValue.setLabel(viewEntity.displayEndValue)
      currentValue.setLabel(viewEntity.displayCurrentValue)
      progressBar.progress = viewEntity.progress
    }
  }
}