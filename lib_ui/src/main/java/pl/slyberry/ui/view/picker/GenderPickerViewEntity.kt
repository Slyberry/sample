package pl.slyberry.ui.view.picker

import pl.slyberry.ui.android.Image

data class GenderPickerViewEntity(
  val leftImage: GenderPickerItemViewEntity,
  val rightImage: GenderPickerItemViewEntity,
  val selectedSide: SelectedSide? = null
) {

  fun currentLeftImage() = when (selectedSide) {
    SelectedSide.LEFT -> leftImage.activeImage
    else -> leftImage.disabledImage
  }

  fun currentRightImage() = when (selectedSide) {
    SelectedSide.RIGHT -> rightImage.activeImage
    else -> rightImage.disabledImage
  }
}

data class GenderPickerItemViewEntity(
  val disabledImage: Image,
  val activeImage: Image
)

enum class SelectedSide {
  LEFT, RIGHT
}
