package pl.slyberry.ui.view.ruler.weight

import android.view.ViewGroup
import pl.slyberry.ui.R
import pl.slyberry.ui.android.setLabel
import pl.slyberry.ui.databinding.WeightRulerItemViewBinding
import pl.slyberry.ui.recyclerview.SimpleRecyclerViewHolder

internal class WeightRulerItemViewHolder(parent: ViewGroup) : SimpleRecyclerViewHolder(R.layout.weight_ruler_item_view, parent) {

  fun applyViewEntity(viewEntity: WeightRulerItemViewEntity) {
    with (WeightRulerItemViewBinding.bind(itemView)) {
      measuredValueTitle.setLabel(viewEntity.amount)
    }
  }
}
