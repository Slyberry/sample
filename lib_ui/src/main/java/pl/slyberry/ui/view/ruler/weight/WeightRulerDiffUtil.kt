package pl.slyberry.ui.view.ruler.weight

import androidx.recyclerview.widget.DiffUtil

internal class WeightRulerDiffUtil(
  private val oldList: List<WeightRulerItemViewEntity>,
  private val newList: List<WeightRulerItemViewEntity>
) : DiffUtil.Callback() {

  override fun getOldListSize() = oldList.size

  override fun getNewListSize() = newList.size

  override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return oldList[oldItemPosition] == newList[newItemPosition]
  }

  override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return oldList[oldItemPosition] == newList[newItemPosition]
  }
}
