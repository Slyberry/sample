package pl.slyberry.ui.view.ruler.height

import androidx.recyclerview.widget.DiffUtil

internal class HeightRulerDiffUtil(
  private val oldList: List<HeightRulerItemViewEntity>,
  private val newList: List<HeightRulerItemViewEntity>
) : DiffUtil.Callback() {

  override fun getOldListSize() = oldList.size

  override fun getNewListSize() = newList.size

  override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return oldList[oldItemPosition] == newList[newItemPosition]
  }

  override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
    return oldList[oldItemPosition] == newList[newItemPosition]
  }
}
