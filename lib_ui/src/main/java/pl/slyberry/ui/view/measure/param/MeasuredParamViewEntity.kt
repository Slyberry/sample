package pl.slyberry.ui.view.measure.param

import pl.slyberry.ui.android.Label

data class MeasuredParamViewEntity(
  val title: Label,
  val value: Label,
  val unit: Label,
  val date: Label
)
