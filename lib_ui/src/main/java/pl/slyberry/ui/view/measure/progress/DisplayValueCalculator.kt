package pl.slyberry.ui.view.measure.progress

import kotlin.math.max
import kotlin.math.min

internal object DisplayValueCalculator {
    fun calculateDisplayValue(current: Float, startValue: Float, endValue: Float): Float {
        return if (startValue < endValue) {
            calculateGain(current, startValue, endValue)
        } else {
            calculateLoss(current, startValue, endValue)
        }
    }

    private fun calculateGain(current: Float, startValue: Float, endValue: Float): Float {
        return min(max(current, startValue), endValue)
    }

    private fun calculateLoss(current: Float, startValue: Float, endValue: Float): Float {
        return max(min(current, startValue), endValue)
    }
}
