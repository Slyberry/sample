package pl.slyberry.ui.view.measure.history

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.ui.android.setBackgroundChroma
import pl.slyberry.ui.android.setLabel
import pl.slyberry.ui.databinding.WeightMeasureItemViewBinding

class WeightMeasureItemView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = WeightMeasureItemViewBinding.inflate(layoutInflater(), this, true)

  fun applyViewEntity(viewEntity: WeightMeasureItemViewEntity) {
    with (binding) {
      weightText.setLabel(viewEntity.weight)
      dateText.setLabel(viewEntity.date)
      diffText.setLabel(viewEntity.diff)
      setBackgroundChroma(viewEntity.background.value)
    }
  }
}
