package pl.slyberry.ui.view.measure.param

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.ui.android.setLabel
import pl.slyberry.ui.databinding.MeasuredParamViewBinding

class MeasuredParamView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = MeasuredParamViewBinding.inflate(layoutInflater(), this, true)

  fun applyViewEntity(viewEntity: MeasuredParamViewEntity) {
    with (binding) {
      titleText.setLabel(viewEntity.title)
      valueText.setLabel(viewEntity.value)
      unitText.setLabel(viewEntity.unit)
      dateText.setLabel(viewEntity.date)
    }
  }
}
