package pl.slyberry.ui.view.ruler.height

import pl.slyberry.ui.android.Label

internal data class HeightRulerItemViewEntity(val amount: Label)
