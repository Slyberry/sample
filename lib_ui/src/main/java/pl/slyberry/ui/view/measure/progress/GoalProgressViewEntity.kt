package pl.slyberry.ui.view.measure.progress

import pl.slyberry.ui.R
import pl.slyberry.ui.android.Label

data class GoalProgressViewEntity(
  private val startValue: Float,
  private val currentValue: Float,
  private val endValue: Float
) {

  val valueInBorders = DisplayValueCalculator.calculateDisplayValue(currentValue, startValue, endValue)

  val displayStartValue = Label.create(R.string.ui_s_value_kg, startValue.toString())
  val displayEndValue = Label.create(R.string.ui_s_value_kg, endValue.toString())
  val displayCurrentValue = createDisplayCurrentValue()
  val progress = ProgressCalculator.calculateProgress(currentValue, startValue, endValue)

  private fun createDisplayCurrentValue(): Label {
    val value = valueInBorders - startValue
    return when {
      value > 0 -> Label.create(R.string.ui_s_positvie_value_kg, (valueInBorders - startValue).toString())
      else -> Label.create(R.string.ui_s_value_kg, (valueInBorders - startValue).toString())
    }
  }
}
