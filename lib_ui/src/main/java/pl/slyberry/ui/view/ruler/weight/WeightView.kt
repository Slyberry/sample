package pl.slyberry.ui.view.ruler.weight

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.ui.R
import pl.slyberry.ui.databinding.WeightViewBinding

class WeightView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = WeightViewBinding.inflate(layoutInflater(), this, true)

  var selectedValue = 0
    private set

  init {
    context.obtainStyledAttributes(attrs, R.styleable.WeightView).also {
      binding.title.text = it.getString(R.styleable.WeightView_weightTitle)
      binding.unit.text = it.getString(R.styleable.WeightView_weightUnit)
    }.recycle()
  }

  fun applyViewEntity(newViewEntity: WeightViewEntity) {
    binding.measuredValue.text = newViewEntity.currentValue.toString()
    selectedValue = newViewEntity.currentValue
    binding.weightRuler.applyViewEntity(newViewEntity) {
      binding.measuredValue.text = it.toString()
      selectedValue = it
    }
  }
}
