package pl.slyberry.ui.view.measure.progress

import kotlin.math.max
import kotlin.math.min

internal object ProgressCalculator {

    fun calculateProgress(current: Float, startValue: Float, endValue: Float): Int {
        return if (startValue < endValue) {
            calculateGain(current, startValue, endValue)
        } else {
            calculateLoss(current, startValue, endValue)
        }
    }

    private fun calculateGain(current: Float, startValue: Float, endValue: Float): Int {
        val diff = endValue - startValue
        val progressValue = min(max(current - startValue, 0f), diff)
        return ((progressValue/diff)*100).toInt()
    }

    private fun calculateLoss(current: Float, startValue: Float, endValue: Float): Int {
        val diff = startValue - endValue
        val progressValue = min(max(startValue - current, 0f), diff)
        return ((progressValue/diff)*100).toInt()
    }
}
