package pl.slyberry.ui.view.picker

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import pl.slyberry.android.common.ext.layoutInflater
import pl.slyberry.ui.android.setImage
import pl.slyberry.ui.databinding.GenderPickerViewBinding

class GenderPickerView(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

  private val binding = GenderPickerViewBinding.inflate(layoutInflater(), this, true)

  lateinit var viewEntity: GenderPickerViewEntity

  fun applyViewEntity(newViewEntity: GenderPickerViewEntity) {
    viewEntity = newViewEntity

    with(binding) {
      setImages(viewEntity)

      leftButton.setOnClickListener {
        viewEntity = viewEntity.copy(selectedSide = SelectedSide.LEFT)
        setImages(viewEntity)
      }

      rightButton.setOnClickListener {
        viewEntity = viewEntity.copy(selectedSide = SelectedSide.RIGHT)
        setImages(viewEntity)
      }
    }
  }

  private fun setImages(viewEntity: GenderPickerViewEntity) {
    with(binding) {
      leftButton.setImage(viewEntity.currentLeftImage())
      rightButton.setImage(viewEntity.currentRightImage())
    }
  }
}
