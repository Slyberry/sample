package pl.slyberry.ui.view.ruler.height

data class HeightViewEntity(val currentValue: Int, val maxValue: Int)
