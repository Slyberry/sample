package pl.slyberry.ui.view.ruler.weight

import pl.slyberry.ui.android.Label

internal data class WeightRulerItemViewEntity(val amount: Label)
