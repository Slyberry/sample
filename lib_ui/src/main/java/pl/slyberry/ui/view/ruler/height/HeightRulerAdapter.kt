package pl.slyberry.ui.view.ruler.height

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pl.slyberry.ui.android.Label
import pl.slyberry.ui.recyclerview.SimpleRecyclerViewHolder

internal class HeightRulerAdapter : RecyclerView.Adapter<SimpleRecyclerViewHolder>() {

  private var items = emptyList<HeightRulerItemViewEntity>()

  fun setNewItemCount(count: Int) {
    val newItems = MutableList(count) { i -> HeightRulerItemViewEntity(Label.create(i.toString())) }
    val result = DiffUtil.calculateDiff(HeightRulerDiffUtil(items, newItems))
    items = newItems
    result.dispatchUpdatesTo(this)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleRecyclerViewHolder {
    return HeightRulerItemViewHolder(parent)
  }

  override fun getItemCount() = Integer.MAX_VALUE //fixme change to some circular layout manager

  override fun onBindViewHolder(holder: SimpleRecyclerViewHolder, position: Int) {
    (holder as HeightRulerItemViewHolder).applyViewEntity(items[position%items.size])
  }
}
