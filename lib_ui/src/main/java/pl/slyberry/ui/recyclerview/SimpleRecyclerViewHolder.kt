package pl.slyberry.ui.recyclerview

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import pl.slyberry.android.common.ext.inflateNoAttach
import pl.slyberry.android.common.ext.layoutInflater

abstract class SimpleRecyclerViewHolder(
  @LayoutRes resId: Int, parent: ViewGroup
) : RecyclerView.ViewHolder(parent.layoutInflater().inflateNoAttach(resId, parent))
