package pl.slyberry.ui.android

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import pl.slyberry.android.common.ext.exhaustive

sealed class Image {

  companion object {
    fun create(@DrawableRes resId: Int): Image = DrawableImageRes(resId)

    fun create(drawable: Drawable): Image = ImageDrawable(drawable)
  }
}

private data class DrawableImageRes(@DrawableRes val resId: Int) : Image()

private data class ImageDrawable(val drawable: Drawable) : Image()

fun ImageView.setImage(image: Image) {
  when (image) {
    is DrawableImageRes -> setImageResource(image.resId)
    is ImageDrawable -> setImageDrawable(image.drawable)
  }.exhaustive
}

fun View.setBackgroundImage(image: Image) {
  when (image) {
    is DrawableImageRes -> setBackgroundResource(image.resId)
    is ImageDrawable -> setBackground(image.drawable)
  }.exhaustive
}

