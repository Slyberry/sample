package pl.slyberry.ui.android

import android.view.View
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat

sealed class Chroma {

  companion object {
    fun create(@ColorRes resId: Int): Chroma = ResChroma(resId)
  }
}

data class ResChroma(@ColorRes val resId: Int) : Chroma()

fun View.setBackgroundChroma(chroma: Chroma) {
  chroma as ResChroma
  setBackgroundColor(ContextCompat.getColor(context, chroma.resId))
}
