package pl.slyberry.ui

import org.junit.Test
import pl.slyberry.ui.view.measure.progress.GoalProgressViewEntity

class GoalProgressViewEntityTest {

    @Test
    fun `when reducing weight by -1 should show value 0`() {
        val viewEntity = GoalProgressViewEntity(62f, 63f, 52f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 0)
        assert(viewEntity.valueInBorders == 62f)
    }


    @Test
    fun `when reducing over limit should show value min and 100%`() {
        val viewEntity = GoalProgressViewEntity(62f, 50f, 52f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 100)
        assert(viewEntity.valueInBorders == 52f)
    }


    @Test
    fun `when reducing weight by 0 should show value 0`() {
        val viewEntity = GoalProgressViewEntity(62f, 62f, 52f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 0)
        assert(viewEntity.valueInBorders == 62f)
    }

    @Test
    fun `when reducing weight by 2 should show value 20(%)`() {
        val viewEntity = GoalProgressViewEntity(62f, 60f, 52f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 20)
        assert(viewEntity.valueInBorders == 60f)
    }

    @Test
    fun `when gaining weight by -1 should show value 0`() {
        val viewEntity = GoalProgressViewEntity(52f, 51f, 62f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 0)
        assert(viewEntity.valueInBorders == 52f)
    }


    @Test
    fun `when gaining weight by 0 should show value 0`() {
        val viewEntity = GoalProgressViewEntity(52f, 52f, 62f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 0)
        assert(viewEntity.valueInBorders == 52f)
    }

    @Test
    fun `when gaining weight by 2 should show value 20(%)`() {
        val viewEntity = GoalProgressViewEntity(52f, 54f, 62f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 20)
        assert(viewEntity.valueInBorders == 54f)
    }

    @Test
    fun `when gaining over limit should show max and 100%`() {
        val viewEntity = GoalProgressViewEntity(52f, 64f, 62f)

        println(viewEntity.progress)
        println(viewEntity.valueInBorders)

        assert(viewEntity.progress == 100)
        assert(viewEntity.valueInBorders == 62f)
    }

}
