package pl.slyberry.buildsrc

object Modules {
    const val APP = ":app"
    const val FEATURE_LOGIN = ":feature_login"
    const val FEATURE_MEASURE = ":feature_measure"
    const val LIB_COMMON = ":lib_common"
    const val LIB_DB = ":lib_db"
    const val LIB_DOMAIN = ":lib_domain"
    const val LIB_REPOSITORY = ":lib_repository"
    const val LIB_UI = ":lib_ui"
}