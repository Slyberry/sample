package pl.slyberry.buildsrc

import org.gradle.api.artifacts.dsl.DependencyHandler

object Dependencies {
  const val LIFECYCLE = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.LIFECYCLE}"
  const val LIFECYCLE_COMMON = "androidx.lifecycle:lifecycle-common-java8:${Versions.LIFECYCLE}"
  const val LIFECYCLE_SCOPE = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.LIFECYCLE}"
  const val LIVE_DATA = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.LIFECYCLE}"

  const val MATERIAL = "com.google.android.material:material:${Versions.MATERIAL}"
  const val COORDINATOR = "androidx.constraintlayout:constraintlayout:${Versions.COORDINATOR}"
  const val LIVE_EVENT = "com.github.hadilq.liveevent:liveevent:${Versions.LIVE_EVENT}"

  const val SWIPE_LAYOUT = "com.chauthai.swipereveallayout:swipe-reveal-layout:${Versions.SWIPE_LAYOUT}"

  const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.COROUTINES}"

  const val RECYCLER_VIEW_SNAP = "com.github.rubensousa:gravitysnaphelper:${Versions.RECYCLER_VIEW_SNAP}"

  const val FLOW_BINDINGS = "io.github.reactivecircus.flowbinding:flowbinding-android:${Versions.FLOW_BINDINGS}"
  const val DATE = "com.jakewharton.threetenabp:threetenabp:${Versions.DATE}"

  object Test {
    const val JUNIT = "junit:junit:${Versions.JUNIT}"
  }

  object Room {
    const val CORE = "androidx.room:room-runtime:${Versions.ROOM}"
    const val COMPILER = "androidx.room:room-compiler:${Versions.ROOM}"
    const val KOTLIN = "androidx.room:room-ktx:${Versions.ROOM}"
    const val TEST = "androidx.room:room-testing:${Versions.ROOM}"
  }

  object Navigation {
    const val FRAGMENT = "androidx.navigation:navigation-fragment-ktx:${Versions.NAVIGATION}"
    const val UI = "androidx.navigation:navigation-ui-ktx:${Versions.NAVIGATION}"
  }

  object Koin {
    const val MAIN = "org.koin:koin-android:${Versions.KOIN}"
    const val SCOPE = "org.koin:koin-android-scope:${Versions.KOIN}"
    const val VIEW_MODEL = "org.koin:koin-android-viewmodel:${Versions.KOIN}"
  }
}

object Versions {
  const val COROUTINES = "1.3.7"
  const val LIFECYCLE = "2.2.0"
  const val MATERIAL = "1.2.0"
  const val COORDINATOR = "2.0.0-beta4"
  const val NAVIGATION = "2.2.2"
  const val KOIN = "2.1.5"
  const val ROOM = "2.2.5"
  const val LIVE_EVENT = "1.2.0"
  const val RECYCLER_VIEW_SNAP = "2.2.0"
  const val FLOW_BINDINGS = "0.11.1"
  const val DATE = "1.2.4"
  const val JUNIT = "4.12"
  const val SWIPE_LAYOUT = "1.4.1"
}

object Libraries {

  fun DependencyHandler.implementKoin() {
    add("implementation", Dependencies.Koin.MAIN)
    add("implementation", Dependencies.Koin.SCOPE)
    add("implementation", Dependencies.Koin.VIEW_MODEL)
  }

  fun DependencyHandler.implementNavigation() {
    add("implementation", Dependencies.Navigation.FRAGMENT)
    add("implementation", Dependencies.Navigation.UI)
  }

  fun DependencyHandler.implementRoom() {
    add("implementation", Dependencies.Room.CORE)
    add("implementation", Dependencies.Room.KOTLIN)
    add("testImplementation", Dependencies.Room.TEST)
    add("kapt", Dependencies.Room.COMPILER)
  }
}
