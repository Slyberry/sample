repositories {
    jcenter()
}

plugins {
    `java-gradle-plugin`
    `kotlin-dsl`
}

gradlePlugin {
    plugins {
        register("shared-gradle-plugin") {
            id = "shared-gradle-plugin"
            implementationClass = "pl.slyberry.buildsrc.SharedGradlePlugin"
        }
    }
}

repositories {
    google()
    mavenCentral()
    jcenter()
}

dependencies {
    compileOnly(gradleApi())

    implementation("com.android.tools.build:gradle:4.1.0")
    implementation("androidx.navigation:navigation-safe-args-gradle-plugin:2.3.0")
    implementation(kotlin("gradle-plugin", "1.4.0"))
    implementation(kotlin("android-extensions"))
}

kotlinDslPluginOptions {
    experimentalWarning.set(false)
}