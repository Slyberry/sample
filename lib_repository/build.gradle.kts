import pl.slyberry.buildsrc.Dependencies
import pl.slyberry.buildsrc.Libraries.implementKoin
import pl.slyberry.buildsrc.Modules

plugins {
    id("com.android.library")
    id("shared-gradle-plugin")
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(project(Modules.LIB_DOMAIN))
    implementation(project(Modules.LIB_COMMON))
    implementation(project(Modules.LIB_DB))

    implementKoin()

    implementation(Dependencies.LIVE_DATA)
    implementation(Dependencies.COORDINATOR)
}
