package pl.slyberry.repository

import org.koin.dsl.module
import pl.slyberry.repository.main.RepositoryImpl
import pl.slyberry.repository.prefs.LocalStorage
import pl.slyberry.repository.usecase.GetAverageBodyParamsUseCase
import pl.slyberry.repository.usecase.GetWeightProgressUseCase

val libRepositoryModule = module {
    single { RepositoryImpl(get(), get()) as Repository }

    single { LocalStorage(get()) }

    factory { GetAverageBodyParamsUseCase(get()) }
    factory { GetWeightProgressUseCase(get()) }
}
