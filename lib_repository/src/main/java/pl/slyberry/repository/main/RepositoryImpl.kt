package pl.slyberry.repository.main

import kotlinx.coroutines.flow.Flow
import pl.slyberry.db.WeightDatabase
import pl.slyberry.domain.BirthDate
import pl.slyberry.domain.Gender
import pl.slyberry.domain.Height
import pl.slyberry.domain.HeightHistoryItem
import pl.slyberry.domain.MeasuredConstants
import pl.slyberry.domain.Name
import pl.slyberry.domain.Weight
import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.domain.WeightGoal
import pl.slyberry.domain.WeightHistoryItem
import pl.slyberry.domain.WeightParams
import pl.slyberry.repository.Repository
import pl.slyberry.repository.prefs.LocalStorage

internal class RepositoryImpl(private val db: WeightDatabase, private val localStorage: LocalStorage) : Repository {

  override fun isInitialized(): Boolean {
    return localStorage.isInitilized
  }

  override fun setIsInitialized() {
    localStorage.isInitilized = true
  }

  override fun getName(): Name {
    return localStorage.name
  }

  override fun saveName(name: Name) {
    localStorage.name = name
  }

  override fun saveWeight(weight: Weight) {
    db.initWeight(weight)
  }

  override fun saveGender(gender: Gender) {
    localStorage.gender = gender
  }

  override fun saveBirthdayDate(birthDate: BirthDate) {
    localStorage.birthDate = birthDate
  }

  override fun saveHeight(height: Height) {
    db.initHeight(height)
  }

  override fun saveGoal(goal: WeightGoal) {
    db.saveGoal(goal)
  }

  override fun getWeightGoal(): Flow<WeightGoal> {
    return db.getCurrentGoal()
  }

  override fun addWeight(weight: Weight) {
    db.saveWeight(weight)
  }

  override fun getRecentWeightParams(): WeightParams {
    return WeightParams(db.getCurrentWeight(), MeasuredConstants.Weight.MAX)
  }

  override fun getWeightHistory(): Flow<List<WeightHistoryItem>> {
    return db.getWeightHistory()
  }

  override fun getWeightAndHeight(): WeightAndHeightStatistics {
    return WeightAndHeightStatistics(
      MeasuredConstants.Weight.AVERAGE,
      MeasuredConstants.Weight.MAX,
      MeasuredConstants.Height.AVERAGE,
      MeasuredConstants.Height.MAX
    )
  }

  override fun getRecentWeightHistoryItem(): Flow<WeightHistoryItem> {
    return db.getRecentWeightHistoryItem()
  }

  override fun getRecentHeightHistoryItem(): Flow<HeightHistoryItem> {
    return db.getRecentHeightHistoryItem()
  }

  override fun getWeightItemCount(): Long {
    return db.getWeightItemCount()
  }

  override fun deleteWeightItem(id: Long) {
    db.deleteWeight(id)
  }
}
