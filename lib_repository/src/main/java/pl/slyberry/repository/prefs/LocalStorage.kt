package pl.slyberry.repository.prefs

import android.content.Context
import pl.slyberry.android.common.ext.requireBoolean
import pl.slyberry.android.common.ext.requireString
import pl.slyberry.android.common.ext.saveBoolean
import pl.slyberry.android.common.ext.saveString
import pl.slyberry.domain.BirthDate
import pl.slyberry.domain.DateWrapper
import pl.slyberry.domain.Gender
import pl.slyberry.domain.Name

internal class LocalStorage(context: Context) {
  private val sharedPreferences = context.getSharedPreferences("local", Context.MODE_PRIVATE)

  private val IS_INITILISED = "IS_INITILISED"
  private val NAME_KEY = "NAME_KEY"
  private val GENDER_KEY = "GENDER_KEY"
  private val BIRTH_DATE_KEY = "BIRTH_DATE_KEY"

  var name: Name
    set(value) = sharedPreferences.saveString(NAME_KEY, value.value)
    get() = Name(sharedPreferences.requireString(NAME_KEY))

  var gender: Gender
    set(value) = sharedPreferences.saveString(GENDER_KEY, value.value)
    get() = Gender.create(sharedPreferences.requireString(GENDER_KEY))

  var birthDate: BirthDate
    set(value) = sharedPreferences.saveString(BIRTH_DATE_KEY, value.value.toString())
    get() = BirthDate(DateWrapper.create(sharedPreferences.requireString(BIRTH_DATE_KEY)))

  var isInitilized: Boolean
    set(value) = sharedPreferences.saveBoolean(IS_INITILISED, value)
    get() = sharedPreferences.requireBoolean(IS_INITILISED)
}
