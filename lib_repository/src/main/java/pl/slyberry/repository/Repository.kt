package pl.slyberry.repository

import kotlinx.coroutines.flow.Flow
import pl.slyberry.domain.BirthDate
import pl.slyberry.domain.Gender
import pl.slyberry.domain.Height
import pl.slyberry.domain.HeightHistoryItem
import pl.slyberry.domain.MeasuredConstants
import pl.slyberry.domain.Name
import pl.slyberry.domain.Weight
import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.domain.WeightGoal
import pl.slyberry.domain.WeightHistoryItem
import pl.slyberry.domain.WeightParams

interface Repository {

    fun isInitialized(): Boolean
    fun setIsInitialized()
    fun getName(): Name

    fun saveName(name: Name)
    fun saveWeight(weight: Weight)
    fun saveGender(gender: Gender)
    fun saveBirthdayDate(birthDate: BirthDate)
    fun saveHeight(height: Height)
    fun saveGoal(goal: WeightGoal)
    fun getWeightGoal(): Flow<WeightGoal>

    fun addWeight(weight: Weight)

    fun getRecentWeightParams(): WeightParams

    fun getWeightHistory(): Flow<List<WeightHistoryItem>>

    fun getWeightAndHeight(): WeightAndHeightStatistics

    fun getRecentWeightHistoryItem(): Flow<WeightHistoryItem>
    fun getRecentHeightHistoryItem(): Flow<HeightHistoryItem>

    fun getWeightItemCount(): Long
    fun deleteWeightItem(id: Long)
}
