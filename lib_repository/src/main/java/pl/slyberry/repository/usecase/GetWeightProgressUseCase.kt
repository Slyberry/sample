package pl.slyberry.repository.usecase

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.zip
import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.WeightProgress
import pl.slyberry.repository.Repository

class GetWeightProgressUseCase(private val repository: Repository) : DefaultUseCase<Unit, Flow<WeightProgress>>() {

  override suspend fun performJob(param: Unit): Flow<WeightProgress> {
    return repository.getRecentWeightHistoryItem().combine(repository.getWeightGoal()) { historyItem, goal ->
      WeightProgress(goal.start, historyItem.weight, goal.target, goal.type)
    }
  }
}
