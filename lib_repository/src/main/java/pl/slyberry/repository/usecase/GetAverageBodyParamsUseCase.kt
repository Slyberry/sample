package pl.slyberry.repository.usecase

import pl.slyberry.android.common.usecase.DefaultUseCase
import pl.slyberry.domain.WeightAndHeightStatistics
import pl.slyberry.repository.Repository

class GetAverageBodyParamsUseCase(private val repository: Repository) : DefaultUseCase<Unit, WeightAndHeightStatistics>() {

  override suspend fun performJob(param: Unit): WeightAndHeightStatistics {
    return repository.getWeightAndHeight()
  }
}
