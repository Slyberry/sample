package pl.slyberry.db.domain

import pl.slyberry.db.entity.GoalEntity
import pl.slyberry.db.entity.GoalType
import pl.slyberry.db.entity.MeasuredParamEntity
import pl.slyberry.db.entity.MeasuredParamType
import pl.slyberry.domain.DateWrapper
import pl.slyberry.domain.Height
import pl.slyberry.domain.Weight
import pl.slyberry.domain.WeightGoal
import pl.slyberry.domain.WeightGoalType

fun Weight.toEntity(diff: Float) = MeasuredParamEntity(
  MeasuredParamType.WEIGHT,
  value,
  DateWrapper.now().serialize(),
  diff,
  0
)

fun Height.toEntity(diff: Float) = MeasuredParamEntity(
  MeasuredParamType.HEIGHT,
  value,
  DateWrapper.now().serialize(),
  diff,
  0
)

fun WeightGoal.toEntity(): GoalEntity {
  return GoalEntity(GoalType.LOSS, start.value, target.value, DateWrapper.now().serialize(), 0)
}

private fun WeightGoalType.toEntity() = when (this) {
  WeightGoalType.LOSS -> GoalType.LOSS
  WeightGoalType.KEEP -> GoalType.KEEP
  WeightGoalType.GAIN -> GoalType.GAIN
}

private fun GoalType.toDomain() = when (this) {
  GoalType.LOSS -> WeightGoalType.LOSS
  GoalType.KEEP -> WeightGoalType.KEEP
  GoalType.GAIN -> WeightGoalType.GAIN
}

fun GoalEntity.toDomain(): WeightGoal {
  return WeightGoal(Weight(start), Weight(target), type.toDomain())
}
