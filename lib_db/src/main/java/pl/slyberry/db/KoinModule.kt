package pl.slyberry.db

import androidx.room.Room
import org.koin.dsl.module
import pl.slyberry.db.database.WeightRoomDatabase
import pl.slyberry.db.room.WeightDatabaseImpl

val libDbModule = module {

  single { Room.databaseBuilder(get(), WeightRoomDatabase::class.java, "weight-database").build() }
  single { WeightDatabaseImpl(get()) as WeightDatabase }

}
