package pl.slyberry.db

import kotlinx.coroutines.flow.Flow
import pl.slyberry.domain.Height
import pl.slyberry.domain.HeightHistoryItem
import pl.slyberry.domain.Weight
import pl.slyberry.domain.WeightGoal
import pl.slyberry.domain.WeightHistoryItem

interface WeightDatabase {

  fun getCurrentWeight(): Weight

  fun initWeight(weight: Weight)

  fun initHeight(height: Height)

  fun saveGoal(goal: WeightGoal)

  fun saveWeight(weight: Weight)

  fun saveHeight(height: Height)

  fun getWeightHistory(): Flow<List<WeightHistoryItem>>

  fun getRecentHeightHistoryItem(): Flow<HeightHistoryItem>

  fun getRecentWeightHistoryItem(): Flow<WeightHistoryItem>

  fun getCurrentGoal(): Flow<WeightGoal>

  fun getWeightItemCount(): Long

  fun deleteWeight(id: Long)
}
