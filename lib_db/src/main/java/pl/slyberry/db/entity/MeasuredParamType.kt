package pl.slyberry.db.entity

enum class MeasuredParamType(val value: String) {
  WEIGHT("WEIGHT"),
  HEIGHT("HEIGHT");

  companion object {
    fun create(value: String): MeasuredParamType {
      return values().find { it.value == value }
        ?: throw IllegalArgumentException("Cannot create type for given value: $value")
    }
  }
}
