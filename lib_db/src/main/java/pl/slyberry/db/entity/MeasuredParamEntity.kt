package pl.slyberry.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "measured_params")
data class MeasuredParamEntity(

  @ColumnInfo(name = "type")
  val type: MeasuredParamType,

  @ColumnInfo(name = "value")
  val value: Float,

  @ColumnInfo(name = "date")
  val date: String,

  @ColumnInfo(name = "diff")
  val diff: Float,

  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "id")
  val id: Long
)
