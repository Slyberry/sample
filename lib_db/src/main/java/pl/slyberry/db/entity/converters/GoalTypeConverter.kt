package pl.slyberry.db.entity.converters

import androidx.room.TypeConverter
import pl.slyberry.db.entity.GoalType

class GoalTypeConverter {

  @TypeConverter
  fun toString(type: GoalType?) = type?.value

  @TypeConverter
  fun fromString(value: String?) = value?.let { GoalType.create(it) }
}
