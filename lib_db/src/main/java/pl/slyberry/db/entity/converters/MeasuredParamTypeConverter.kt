package pl.slyberry.db.entity.converters

import androidx.room.TypeConverter
import pl.slyberry.db.entity.MeasuredParamType

class MeasuredParamTypeConverter {

  @TypeConverter
  fun toString(type: MeasuredParamType?) = type?.value

  @TypeConverter
  fun fromString(value: String?) = value?.let { MeasuredParamType.create(it) }
}
