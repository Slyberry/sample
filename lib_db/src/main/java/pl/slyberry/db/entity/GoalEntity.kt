package pl.slyberry.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "goals")
data class GoalEntity(

  @ColumnInfo(name = "type")
  val type: GoalType,

  @ColumnInfo(name = "start")
  val start: Float,

  @ColumnInfo(name = "target")
  val target: Float,

  @ColumnInfo(name = "date")
  val date: String,

  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "id")
  val id: Long
)
