package pl.slyberry.db.entity

enum class GoalType(val value: String) {
  KEEP("KEEP"),
  GAIN("GAIN"),
  LOSS("LOSS");

  companion object {
    fun create(value: String): GoalType {
      return values().find { it.value == value }
        ?: throw IllegalArgumentException("Cannot create type for given value: $value")
    }
  }
}
