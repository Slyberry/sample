package pl.slyberry.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import pl.slyberry.db.entity.GoalEntity
import pl.slyberry.db.entity.MeasuredParamEntity
import pl.slyberry.db.entity.MeasuredParamType

@Dao
internal interface MeasuredParamsDao {

  @Insert
  fun insert(params: GoalEntity)

  @Insert
  fun insert(params: MeasuredParamEntity)

  @Query("""SELECT MAX(date) FROM measured_params WHERE type = :measuredParamType""")
  fun getParamMaxDate(measuredParamType: MeasuredParamType): Long

  @Query("""SELECT *, MAX(date) FROM measured_params WHERE type = :measuredParamType""")
  fun getCurrentParam(measuredParamType: MeasuredParamType): MeasuredParamEntity

  @Query("""SELECT *, MAX(date) FROM measured_params WHERE type = :measuredParamType""")
  fun getCurrentParamFlow(measuredParamType: MeasuredParamType): Flow<MeasuredParamEntity>

  @Query("""SELECT * FROM measured_params WHERE type = :measuredParamType ORDER BY date DESC""")
  fun getParamsHistory(measuredParamType: MeasuredParamType): Flow<List<MeasuredParamEntity>>

  @Query("""SELECT *, MAX(id) FROM goals""")
  fun getCurrentGoal(): Flow<GoalEntity>

  @Query("""SELECT COUNT(*) FROM measured_params WHERE type = "WEIGHT"""")
  fun itemCount(): Long

  @Query("""DELETE FROM measured_params WHERE id = :id""")
  fun deleteWeight(id: Long)
}
