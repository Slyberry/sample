package pl.slyberry.db.room

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import pl.slyberry.db.WeightDatabase
import pl.slyberry.db.database.WeightRoomDatabase
import pl.slyberry.db.domain.toDomain
import pl.slyberry.db.domain.toEntity
import pl.slyberry.db.entity.MeasuredParamType
import pl.slyberry.domain.DateWrapper
import pl.slyberry.domain.Height
import pl.slyberry.domain.HeightHistoryItem
import pl.slyberry.domain.Weight
import pl.slyberry.domain.WeightDiff
import pl.slyberry.domain.WeightGoal
import pl.slyberry.domain.WeightHistoryItem

internal class WeightDatabaseImpl(private val db: WeightRoomDatabase) : WeightDatabase {

  override fun getCurrentWeight(): Weight {
    return db.measuredParamsDao().getCurrentParam(MeasuredParamType.WEIGHT).let {
      Weight(it.value, it.id)
    }
  }

  override fun initWeight(weight: Weight) {
    db.measuredParamsDao().insert(weight.toEntity(0f))
  }

  override fun initHeight(height: Height) {
    db.measuredParamsDao().insert(height.toEntity(0f))
  }

  override fun saveGoal(goal: WeightGoal) {
    db.measuredParamsDao().insert(goal.toEntity())
  }

  override fun saveWeight(weight: Weight) {
    val diff = weight.value - (db.measuredParamsDao().getCurrentParam(MeasuredParamType.WEIGHT).value)
    db.measuredParamsDao().insert(weight.toEntity(diff))
  }

  override fun saveHeight(height: Height) {
    val diff = height.value - (db.measuredParamsDao().getCurrentParam(MeasuredParamType.HEIGHT).value)
    db.measuredParamsDao().insert(height.toEntity(diff))
  }

  override fun getWeightHistory(): Flow<List<WeightHistoryItem>> {
    return db.measuredParamsDao().getParamsHistory(MeasuredParamType.WEIGHT).map {
      it.map { WeightHistoryItem(Weight(it.value), DateWrapper.deserialize(it.date), WeightDiff(it.diff), it.id) }
    }
  }

  override fun getRecentHeightHistoryItem(): Flow<HeightHistoryItem> {
    return db.measuredParamsDao().getCurrentParamFlow(MeasuredParamType.HEIGHT).map {
      HeightHistoryItem(Height(it.value), DateWrapper.deserialize(it.date), WeightDiff(it.diff), it.id)
    }
  }

  override fun getRecentWeightHistoryItem(): Flow<WeightHistoryItem> {
    return db.measuredParamsDao().getCurrentParamFlow(MeasuredParamType.WEIGHT).map {
      WeightHistoryItem(Weight(it.value), DateWrapper.deserialize(it.date), WeightDiff(it.diff), it.id)
    }
  }

  override fun getCurrentGoal(): Flow<WeightGoal> {
    return db.measuredParamsDao().getCurrentGoal().map {
      it.toDomain()
    }
  }

  override fun getWeightItemCount(): Long {
    return db.measuredParamsDao().itemCount()
  }

  override fun deleteWeight(id: Long) {
    db.measuredParamsDao().deleteWeight(id)
  }
}
