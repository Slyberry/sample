package pl.slyberry.db.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pl.slyberry.db.dao.MeasuredParamsDao
import pl.slyberry.db.entity.GoalEntity
import pl.slyberry.db.entity.MeasuredParamEntity
import pl.slyberry.db.entity.converters.GoalTypeConverter
import pl.slyberry.db.entity.converters.MeasuredParamTypeConverter

@Database(
  entities = [GoalEntity::class, MeasuredParamEntity::class],
  version = 1
)
@TypeConverters(
  GoalTypeConverter::class,
  MeasuredParamTypeConverter::class
)
internal abstract class WeightRoomDatabase : RoomDatabase() {

  abstract fun measuredParamsDao(): MeasuredParamsDao
}
