import pl.slyberry.buildsrc.Libraries.implementKoin
import pl.slyberry.buildsrc.Libraries.implementRoom
import pl.slyberry.buildsrc.Modules

plugins {
    id("com.android.library")
    id("shared-gradle-plugin")
    id("kotlin-kapt")
}

dependencies {
    implementRoom()
    implementKoin()
    implementation(project(Modules.LIB_DOMAIN))
}
